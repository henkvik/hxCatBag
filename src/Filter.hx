
import wow.G.LoadStringVal;
import widget.FilterEdit;
import wow.widget.Texture.AlphaMode;
import wow.widget.Region.Point;
import haxe.Constraints.Function;
import wow.widget.Button;
import wow.widget.FontString;
import wow.widget.Frame;
import wow.widget.Texture.TexturePath;
import Slot.SlotData;

using Style.StyleEx;


class FilterData {
    public var name        : String      = "";
    public var code        : String      = "";
    public var icon        : TexturePath = "";
    public var is_open     : Bool        = true;
    public var is_hidden   : Bool        = false;
    public var auto_open   : Bool        = false;
    public var auto_close  : Bool        = false;
    public var auto_hide   : Bool        = true;
    public var volatile    : Bool        = false;
    public var old_size    : Int         = 0;
    public function new() return;
}


class Filter {
    public var data : FilterData;

    public var bag : Bag;
    var func  : SlotData->Bool;
    var slots : Array<Slot>;

    public var frame(default, null) : Frame;
    var button   : Button;
    var title    : FontString;
    var children : Frame;

    public function new(name:String, icon:TexturePath, code:String) {
        slots = new Array<Slot>();

        data = new FilterData();
        data.name = name;
        data.icon = icon;
        set_code(code);

        create_frame();
    }

    public function filter_slots(u_slots : Array<Slot>) {
        if (data.volatile) set_code(data.code);
        slots.resize(0);
        
        var f_slots = new Array<Slot>();
        
        try {
            for (slot in u_slots) {
                var func = func;
                if (cast Code.Execute(func, slot.data)) {
                    slots.push(slot);
                } else {
                    f_slots.push(slot);
                }
            }
        } catch (se:ScriptError) {
            untyped __lua__("print({0})",'Catbag Runtime Error:\n  Filter: "$name"\n  Line: ${se.line}\n  Error: ${se.msg}');
            slots.resize(0);
            return u_slots;
        }

        if (data.auto_hide)  data.is_hidden = slots.length == 0;
        if (data.auto_open)  data.is_open   = !data.is_open ? slots.length != data.old_size  : data.is_open;
        if (data.auto_close) data.is_open   =  data.is_open ? slots.length != 0              : data.is_open;
        data.old_size = slots.length;

        return f_slots;
    }

    public function anchor(point :Point, parent:Frame, p_point :Point, x:Float, y:Float) {
        frame.ClearAllPoints();
        frame.SetParent(parent);
        frame.SetPoint(point, parent, p_point, x, y);
    }

    public function get_height() {
        return frame.GetHeight();
    }

    public function redraw(columns : Int) {
        frame.SetShown(!data.is_hidden);

        if (data.is_open) {
            frame.SetSize(
                Style.calc_width(columns), 
                Style.calc_height(Math.ceil(slots.length / columns))
            );
            children.Show();
            title.Show();
        } else {
            frame.SetSize(Style.calc_height(0), Style.calc_height(0));
            children.Hide();
            title.Hide();
        }

        var i = 0;
        for (slot in slots) {
            slot.redraw();
            slot.anchor(TOPLEFT, children, TOPLEFT, 
                 Math.floor(i % columns) * (Style.SLOT_SIZE + Style.SLOT_PAD), 
                -Math.floor(i / columns) * (Style.SLOT_SIZE + Style.SLOT_PAD)
            );
            i++;
        }

        title.SetText(data.name);
        button.SetNormalTexture(data.icon);
        button.SetHighlightTexture(data.icon);
    }

    public function hide() {
        frame.Hide();
    }

    public function set_code(code :String) {
        this.func = cast Code.Compile(code);
        data.code = code;
        return code;
    }

    static var EMPTY_SLOT = new SlotData(-1,-1);
    public static function validate_code(code :String):ScriptError {

        try {
            Code.Execute(Code.Compile(code), EMPTY_SLOT);
        } catch (se:ScriptError) {
            return se;
        }

        return null;
    }

    function create_frame() {
        frame = wow.G.CreateFrame(FRAME);
        
        frame.SetSize(Style.calc_width(0), Style.calc_height(0));
        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);

        button = wow.G.CreateFrame(BUTTON, null, frame);
        button.SetSize(Style.ICON_SIZE, Style.ICON_SIZE);
        button.SetPoint(TOPLEFT, frame, TOPLEFT, Style.PAD, -Style.PAD);

        button.SetNormalTexture(data.icon);
        button.SetHighlightTexture(data.icon, AlphaMode.ADD);
        button.GetNormalTexture().ExSetTexCoordInset(Style.ICON_INSET);
        button.GetHighlightTexture().ExSetTexCoordInset(Style.ICON_INSET);

        button.RegisterForClicks(LeftButtonUp, RightButtonUp);
        button.SetScript("OnClick", (_, btn)->{
            switch btn {
                case LeftButton: 
                    data.is_open = !data.is_open;
                    bag.redraw();
                case RightButton:
                    widget.FilterEdit.INSTANCE.edit(this);
                case _:
            }
        });

        title = frame.CreateFontString(null, ARTWORK);
        title.SetPoint(LEFT, button, RIGHT, Style.PAD, 0);
        title.SetFont(Style.FONT, Style.FONT_SIZE);
        title.SetText(data.name);

        children = wow.G.CreateFrame(FRAME, null, frame);
        children.SetSize(0.001, 0.001);
        children.SetPoint(TOPLEFT, button, BOTTOMLEFT, 0, -Style.PAD);

        var use_all:Button = wow.G.CreateFrame(BUTTON, null, children);
        use_all.Hide();
        use_all.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);
        use_all.SetSize(Style.ICON_SIZE, Style.ICON_SIZE);
        use_all.SetNormalTexture("Interface/MINIMAP/Vehicle-AllianceMagePortal");
        use_all.SetHighlightTexture("Interface/MINIMAP/Vehicle-AllianceMagePortal");

        use_all.SetScript("OnClick", ()->{
            if (!wow.G.InCombatLockdown()) {
                var i = 0;
                var time = wow.G.GetTime();
                use_all.SetScript("OnUpdate", ()->{
                    if (i < slots.length) {
                        var now = wow.G.GetTime();
                        if ((now - time) > 0.001) {
                            trace(i);
                            slots[i++].use();
                            time = now; 
                        }
                    } else {
                        use_all.SetScript("OnUpdate", null);
                    }
                });                
            }
        });
        
        frame.SetScript("OnEnter",()->{
            use_all.Show();
            frame.SetScript("OnUpdate", ()->{
                if (!frame.IsMouseOver()) {
                    frame.SetScript("OnUpdate", null);
                    use_all.Hide();
                }
            });
        });

    }

    static public function load(data:FilterData) : Filter {
        var filter = new Filter(data.name, data.icon, data.code);
        filter.data = data;
        return filter;
    }
}

class Code {
    static public function Compile(code :String, ?name :String) : Function {
        try {
            var ret = wow.G.loadstring(code, name);
            var func = ret.func;

            if (func == null) {
                throw new ScriptError("Syntax", ret.err);
            }
           
            return func();
        } catch (err:String) {
            throw new ScriptError("Compile", err);
        }
        return ()->false;
    }

    public static function Execute(func :Function, param :Any) : Any {
        var ret = lua.Lua.pcall(func, param);

        if (!ret.status) {
            throw new ScriptError("Runtime", ret.value);
        }    

        return ret.value;    
    }
}

class ScriptError {
    public var type(default, null) : String;
    public var name(default, null) : String;
    public var line(default, null) : Int;
    public var msg(default, null)  : String;

    public function new(type:String, err :String) {
        untyped __lua__("local name, line, msg = {0}:match('%[string (\".-\")%]:(%d+): (.*)')", err);

        this.type = type;
        name = untyped __lua__("name");
        line = untyped __lua__("line");
        msg  = untyped __lua__("msg");
    }
}
