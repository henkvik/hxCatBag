package widget;

import lua.Table.AnyTable;
import wow.widget.FontString;
using Style.StyleEx;

class SlotInfo extends Window {
    static public final INSTANCE = new SlotInfo();

    var fstrings : Map<String, FontString>;

    public function new() {
        super("Slot Info", 250, 100);
        var g:AnyTable = untyped __lua__("_G");
        g.ContainerFrame1.HookScript("OnHide", () -> {hide();});
        fstrings = new Map<String, FontString>();
        
        create_row("bag_id");
        create_row("slot_id");

        new_row();
        create_row("icon");
        create_row("link");

        new_row();
        create_row("name");
        create_row("type");
        create_row("sub_type");
        create_row("equip_loc");
        create_row("tooltip");

        new_row();
        create_row("level");
        create_row("required_level");
        create_row("count");
        create_row("stack_size");
        create_row("sell_price");

        new_row();
        create_row("id");
        create_row("type_id");
        create_row("sub_type_id");
        create_row("bind_id");
        create_row("quality_id");
        create_row("expac_id");
        create_row("set_id");
        create_row("equip_ids");

        new_row();
        create_row("is_locked");
        create_row("is_readable");
        create_row("is_lootable");
        create_row("is_reagent");
        create_row("is_equippable");
        create_row("is_usable");
        create_row("is_consumable");

        new_row();
        create_row("can_use");
        create_row("empty");

        update_height();
    }

    public function create_row(name:String) {
        var row = new_row();

        var text = row.CreateFontString();
        text.SetPoint(LEFT);
        text.ExSetFont();
        text.SetText(name);
        
        var fstring = row.CreateFontString();
        // fstring.SetPoint(LEFT, text, RIGHT, Style.PAD, 0);
        fstring.SetWidth(150);
        fstring.ExSetFont();
        fstring.SetPoint(RIGHT);
        fstring.SetJustifyH(RIGHT);
        fstring.SetHeight(Style.FONT_SIZE);

        row.SetScript("OnMouseUp", ()->{
            wow.G.print(fstring.GetText());
        });

        fstrings.set(name, fstring);
    } 

    public function view(item :Slot.SlotData) {

        set_text("bag_id",         item.bag_id);
        set_text("slot_id",        item.slot_id);

        set_text("icon",           item.icon);
        set_text("link",           item.link);

        set_text("name",           item.name);
        set_text("type",           item.type);
        set_text("sub_type",       item.sub_type);
        set_text("equip_loc",      item.equip_loc);
        set_text("tooltip",        item.tooltip);

        set_text("level",          item.level);
        set_text("required_level", item.required_level);
        set_text("count",          item.count);
        set_text("stack_size",     item.stack_size);
        set_text("sell_price",     item.sell_price);

        set_text("id",             item.id);
        set_text("type_id",        item.type_id);
        set_text("sub_type_id",    item.sub_type_id);
        set_text("bind_id",        item.bind_id);
        set_text("quality_id",     item.quality_id);
        set_text("expac_id",       item.expac_id);
        set_text("set_id",         item.set_id);
        set_text("equip_ids",      item.equip_ids);

        set_text("is_locked",      item.is_locked);
        set_text("is_readable",    item.is_readable);
        set_text("is_lootable",    item.is_lootable);
        set_text("is_reagent",     item.is_reagent);
        set_text("is_equippable",  item.is_equippable);
        set_text("is_usable",      item.is_usable);
        set_text("is_consumable",  item.is_consumable);

        set_text("can_use",        item.can_use);
        set_text("empty",          item.empty);    

        show();
    }

    function set_text(string:String, value:Any) {
        var fstring = fstrings[string];

        switch Type.typeof(value) {
            case TInt: 
                fstring.SetText(value);
                fstring.ExSetTextColor(Style.COLOR_NUMBER);
                
            case TBool: 
                fstring.SetText(Std.string(value));
                fstring.ExSetTextColor(Style.COLOR_BOOL);
            case _:
                switch Type.getClass(value) {
                    case String:
                        fstring.SetText('"' + value + '"');
                        fstring.ExSetTextColor(Style.COLOR_STRING);
                    case _:
                        fstring.SetText(Std.string(value));
                        fstring.ExSetTextColor(Style.COLOR_NUMBER);
                }

        }
    }
}