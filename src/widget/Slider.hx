package widget;

import haxe.Constraints.Function;
import wow.widget.Frame;

using Style.StyleEx;

@:forward
abstract Slider(Frame) to Frame {
    public function new(parent : Frame) {
        this = wow.G.CreateFrame(FRAME, null, parent);
        this.SetSize(20, 200);

        (cast this).min = 0;
        (cast this).max = 0;
        (cast this).value = 0;
        (cast this).max_offset = 0;

        create_thumb();

        set_min_max(0, 100);
        set_thumb_size(50);
        set_value(0);
    }

    function create_thumb() {
        var me = this;
        var thumb:Frame = wow.G.CreateFrame(FRAME, null, this);
        thumb.SetPoint(LEFT);
        thumb.SetPoint(RIGHT);
        thumb.SetPoint(TOP);
        thumb.SetBackdrop(Style.BACKDROP);
        thumb.ExSetBackdropColor(Style.BUTTON_NORMAl);

        (cast this).offset = 0;
        thumb.SetScript("OnMouseDown", ()->{
            var scale = wow.G.UIParent.GetScale();
            var start_y = wow.G.GetCursorPosition().y;

            var t = this;
            thumb.SetScript("OnUpdate",()->{
                var delta = start_y - wow.G.GetCursorPosition().y;
                delta /= scale;

                if (Math.abs(delta) > 1) {
                    start_y = wow.G.GetCursorPosition().y;
                    set_offset(this, (cast this).offset + delta);
                    set_value_from_offset(this);
                }
            });
        });

        thumb.SetScript("OnMouseUp", ()->{
            thumb.SetScript("OnUpdate", null);
        });

        (cast this).thumb = thumb;
    }

    public function set_value_callback(callback:Function) {
        (cast this).value_callback = callback;
    }

    function set_offset(offset:Float) {
        var max_offset = (cast this).max_offset;
        offset = Math.max(0, Math.min(max_offset, offset));
        (cast this).offset = offset;

        var thumb:Frame = (cast this).thumb;
        thumb.SetPoint(TOP, this, TOP, 0, -offset);
    }

    function set_value_from_offset() {
        var factor = (cast this).offset / (cast this).max_offset;
        var min = (cast this).min_value;
        var max = (cast this).max_value;
        var value = min + (max - min) * factor;
        (cast this).value = value;

        var fn:Function = (cast this).value_callback;
        if (fn != null) fn(value);
    }

    public function set_thumb_size(size : Float) {
        (cast this).thumb.SetHeight(size);
        (cast this).max_offset = this.GetHeight() - size;
    }

    public function set_min_max(min:Float, max:Float) {
        (cast this).min_value = min;
        (cast this).max_value = max;

        var value = (cast this).value;
        var factor = (value - min) / (max - min);

        set_offset((cast this).max_offset * factor);
        set_value_from_offset();
    }

    public function get_value():Float {
        return (cast this).value;
    }

    public function set_value(value : Float) {
        var min = (cast this).min_value;
        var max = (cast this).max_value;
        value = Math.min(max, Math.max(min, value));
        (cast this).value = value;

        var factor = (value - min) / (max - min);

        set_offset((cast this).max_offset * factor);

        var fn:Function = (cast this).value_callback;
        if (fn != null) fn(value);
    }
}