package widget;

import haxe.Constraints.Function;
import wow.widget.FontInstance.FontJustifyH;
import wow.StringApi;
import wow.widget.Frame;

using Style.StyleEx;

@:forward
abstract CodeEditBox(Frame) to Frame {
    static final slider_width = 10.0;

    public function new(parent:Frame) {
        this = wow.G.CreateFrame(FRAME, null, parent);
        this.SetSize(200,200);
        this.SetClipsChildren(true);
        this.Show();

		this.SetBackdrop(Style.BACKDROP);
		this.ExSetBackdropColor(Style.EDIT_BACKGROUND);
        
		create_slider();
        create_editbox();

    }


    public function get_code() {
        return (cast this).edit.GetText();
    }

    public function set_code(code:String) {
        return (cast this).edit.SetText(code);
    }

    public function on_code_changed(callback:Function) {
        (cast this).code_changed_callback = callback;
    }

    public function clear_focus() {
        (cast this).edit.ClearFocus();
    }

    public function set_cursor(position:Int){
        (cast this).edit.SetCursorPosition(position);
    }

    function create_editbox() {
        var slider:Slider = (cast this).slider;
        var edit = new TextEdit(this);
        edit.SetBackdrop(null);
        edit.SetPoint(TOPLEFT);
        edit.SetPoint(TOPRIGHT, slider, TOPLEFT);
        edit.SetMultiLine(true);
        edit.SetTextColor(0,0,0,0);
        edit.SetFont(Style.CODE_FONT, Style.FONT_SIZE);

        var color_text = edit.CreateFontString();
        var f = edit.GetFont();
        color_text.SetFont(f.name, f.height);
        color_text.SetAllPoints(cast edit);
        color_text.SetJustifyH(FontJustifyH.LEFT);


        this.SetScript("OnMouseDown", ()->edit.SetFocus());

        var cursor:Frame = wow.G.CreateFrame(FRAME, null, edit);
        cursor.SetSize(f.height*0.1, f.height);
        cursor.SetBackdrop(Style.BACKDROP);
        cursor.Hide();

        edit.SetScript("OnEditFocusGained", ()->{
            cursor.Show();
            var old = wow.G.GetTime();
            edit.SetScript("OnUpdate", ()->{
                var delta = wow.G.GetTime() - old;
                if (delta > 0.5) {
                    cursor.SetShown(!cursor.IsShown());
                    old = wow.G.GetTime();
                }
            });
        });

        edit.SetScript("OnEditFocusLost", ()->{
            cursor.Hide();
            edit.SetScript("OnUpdate", null);
        });

        edit.EnableMouseWheel(true);
        edit.SetScript("OnMouseWheel", (_, d)->{
            slider.set_value(slider.get_value() - d * f.height);
        });

        edit.SetScript("OnCursorChanged", (_, x, y, _, line_height)->{
            var value = slider.get_value();
            var height = this.GetHeight();
            var pos = (-y) - value;

            cursor.SetPoint(TOPLEFT, edit, TOPLEFT, x, y);

            if (pos < 0) {
                slider.set_value(value + pos);
            } else if (pos > height - line_height) {
                slider.set_value(value + pos - (height - line_height));
            }
        });

        edit.SetScript("OnTextChanged", (_, is_user)->{
            var edit_height = edit.GetHeight();
            var frame_height = this.GetHeight();
            slider.set_thumb_size(frame_height * Math.min(1, frame_height /  edit_height));
            slider.set_min_max(0, Math.max(0, edit_height - frame_height));

            var code = get_code();
            color_text.SetText(color_code(code));
            var fn = (cast this).code_changed_callback;
            if (fn != null) fn(code);
        });

        edit.HookScript("OnTabPressed", ()->{
            edit.Insert("  ");
        });

        slider.set_value_callback((value)->{
            edit.SetPoint(TOPLEFT, this, TOPLEFT, 0, value);
        });

        (cast this).edit = edit;   
    }

    function create_slider() {
        var slider = new Slider(this);
		slider.SetWidth(slider_width);
        slider.SetPoint(BOTTOMRIGHT);
        slider.SetPoint(TOPRIGHT);

        (cast this).slider = slider;
    }

    function color_code(code :String):String {

        var colors = new Map<String,String>();
        colors.set("normal"   , "|c00ffffff");
        colors.set("const"    , "|c00569cd6");
        colors.set("number"   , "|c00b5cea8");
        colors.set("string"   , "|c00ce9178");
        colors.set("keyword"  , "|c00C586C0");
        colors.set("function" , "|c00DCDCAA");
        colors.set("delimiter", "|c00ffffff");

        var chunks = parse_code_chunks(code);
        find_special_chunks(chunks);
        
        var colored = "";
        for (chunk in chunks) {
            var color = colors.get(chunk.type);
            if (color != null) {
                colored += color + chunk.value;
                
            } else {
                throw "type "+ chunk.type + " has no color";
            }
        }

        return colored;
    }

    function parse_code_chunks(code):Array<CodeChunk> {
        var chunks = [];

        var delimiters = [
            "+", "-", "*", "/", "%", "^",
            "(", ")", "{", "}", "[", "]",
            "=", "~", "<", ">", "#", " ",
            ";", ":", ",", ".", "\n",
        ];

        var is_delimiter = (char:String)->{
            for (delimiter in delimiters) {
                if (char == delimiter) {
                    return true;
                }
            }
            return false;
        };
                
        var current = { type:"normal", value:"" };
        var prev = "";
        for (i in 0...code.length) {
            var char = code.charAt(i);

            if (char == '"' && prev != '\\') {
                if (current.type == "string") {
                    current.value += '"';
                    chunks.push(current);
                    current = { type:"normal", value: "" };
                } else {
                    chunks.push(current);
                    current = { type:"string", value: char };
                }
            } else if (is_delimiter(char) && current.type != "string") {
                chunks.push(current);
                chunks.push({type: "delimiter", value: char});
                current = { type:"normal", value: "" };
            } else {
                current.value += char;
            }

            prev = char;
        }

        chunks.push(current);
        
        return chunks;
    }

    function find_special_chunks(chunks:Array<CodeChunk>) {
        var keywords = [
            "and","break","do","else","elseif","end",
            "for","function","if","in","local","not",
            "or","repeat","return","then","until","while"
        ];


        var is_keyword = (word:String)->{
            for (keyword in keywords) {
                if (word == keyword) return true;
            }
            return false;
        };

        var consts = ["true","false","nil"];

        var is_const = (word:String)->{
            for (const in consts) {
                if (word == const) return true;
            }
            return false;
        };

        var empty = { type:"", value:"" };
        for (i in 0...chunks.length) {
            // var prev = (i > 0) ? chunks[i-1] : empty;
            var chunk = chunks[i];
            var next = (i+1 < chunks.length) ? chunks[i+1] : empty;

            if (chunk.type == "normal") {

                if (next.value == "(") {
                    chunk.type = "function";
                } 

                if (wow.G.tonumber(chunk.value) != null) {
                    chunk.type = "number";
                } 

                if (is_const(chunk.value)) {
                    chunk.type = "const";
                } else if (is_keyword(chunk.value)){
                    chunk.type = "keyword";
                } 
            }
        }

        for (i in 0...chunks.length) {
            // var prev = (i > 0) ? chunks[i-1] : empty;
            var chunk = chunks[i];
            var next = (i+1 < chunks.length) ? chunks[i+1] : empty;

            if (chunk.type == "delimiter") {
                if (next.type == "number") {
                    chunk.type = "number";
                }
            }
        }
    }
}

typedef CodeChunk = { type:String, value:String };