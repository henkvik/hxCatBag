package widget;

import wow.widget.Font;
import wow.widget.Frame;
import wow.widget.Button;

using Style.StyleEx;

@:forward
abstract TextButton(Button) to Button {
    
    public function new(text:String, parent: Frame) {
        this = wow.G.CreateFrame(BUTTON, null, parent);
        this.SetText(text);
        this.SetHeight(Style.FONT_SIZE);
        this.SetBackdrop(Style.BACKDROP);
        this.ExSetBackdropColor(Style.BUTTON_NORMAl);
        // this.SetFrameLevel(parent.GetFrameLevel() + 1);

        var fnt = this.GetFontString();
        fnt.SetFont(Style.FONT, Style.FONT_SIZE);
        fnt.ClearAllPoints();
        fnt.SetPoint(CENTER);
        this.SetWidth(this.GetFontString().GetWidth() + Style.PAD * 2);

    }

    public function set_color(color:Style.Color) {
        this.ExSetBackdropColor(color);
    }
}