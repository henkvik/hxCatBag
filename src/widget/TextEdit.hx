package widget;

import wow.widget.Frame;
import wow.widget.EditBox;
using Style.StyleEx;

@:forward
abstract TextEdit(EditBox) to EditBox {
    public function new(parent:Frame) {
        this = wow.G.CreateFrame(EDITBOX, null, parent);
        this.SetFont(Style.FONT, Style.FONT_SIZE);
        this.SetHeight(Style.FONT_SIZE);
        this.SetAutoFocus(false);
        this.SetScript("OnEscapePressed", this.ClearFocus);
        this.SetBackdrop(Style.BACKDROP);
        this.ExSetBackdropColor(Style.EDIT_BACKGROUND);
    }
}