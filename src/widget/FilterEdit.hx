package widget;

import lua.Table.AnyTable;
import wow.widget.Button;
import wow.widget.LayeredRegion.DrawLayer;
import wow.widget.FontString;
import wow.widget.Frame;
import wow.widget.EditBox;
using Style.StyleEx;

class FilterEdit {
    var open : Bool;

    var frame : Frame;
    var content : Frame;
    var rows : Array<Frame>;
    var inits : Array<Void->Void>;

    public var filter:Filter;
    


    public function new() {
        rows  = new Array<Frame>();
        inits = new Array();

        create_frame();

        create_row_state();        
        create_row_automation();        
        create_row_name();
        create_row_icon();
        create_row_code();
    }

    function create_frame() {
        frame = wow.G.CreateFrame(FRAME, null, wow.G.UIParent);
        frame.SetSize(300, 350);
        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        frame.SetPoint(CENTER, wow.G.UIParent, CENTER);
        frame.SetToplevel(true);
        frame.SetResizable(true);
        frame.SetMinResize(280, 250);
        frame.Hide();
        
        // make draggable
        frame.SetMovable(true);
        frame.EnableMouse(true);
        frame.RegisterForDrag(LeftButton);
        frame.SetScript("OnDragStart", frame.StartMoving);
        frame.SetScript("OnDragStop", frame.StopMovingOrSizing);

 


        var title = frame.CreateFontString();
        title.SetPoint(TOP, frame, TOP, 0, -Style.PAD);
        title.SetFont(Style.FONT, Style.FONT_SIZE);
        title.SetText("Category Settings");

        var close = new TextButton("X", frame);
        close.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);
        close.SetScript("OnClick",()->{
            hide();
        });

        content = wow.G.CreateFrame(FRAME, null, frame);
        content.SetPoint(TOP,    title, BOTTOM, 0, -Style.PAD);  
        content.SetPoint(BOTTOM, frame, BOTTOM, 0,  Style.PAD);
        content.SetPoint(LEFT,   frame, LEFT,   Style.PAD,  0);    
        content.SetPoint(RIGHT,  frame, RIGHT, -Style.PAD,  0);

        var resize:Button = wow.G.CreateFrame(BUTTON, null, frame);
        resize.SetSize(16,16);
        resize.SetPoint(BOTTOMRIGHT);
        resize.SetNormalTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Up");
        resize.SetPushedTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Down");
        resize.SetHighlightTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Highlight");

        resize.RegisterForClicks(LeftButtonUp);
        resize.SetScript("OnMouseDown", ()-> { 
            frame.StartSizing(BOTTOMRIGHT);
            resize.GetHighlightTexture().Hide();
        });
        resize.SetScript("OnMouseUp", ()->{
            frame.StopMovingOrSizing();
            resize.GetHighlightTexture().Show();
        });

        var g:AnyTable = untyped __lua__("_G");
        g.ContainerFrame1.HookScript("OnHide", () -> {hide();});
    }

    function new_row() {
        var row:Frame = wow.G.CreateFrame(FRAME, null, content);
        row.SetHeight(Style.FONT_SIZE);
        row.SetPoint(RIGHT);
        row.Show();

        var row_id = rows.push(row) - 1;
        if (row_id == 0) {
            row.SetPoint(TOPLEFT);
        } else {
            row.SetPoint(TOPLEFT, rows[row_id-1], BOTTOMLEFT, 0, -Style.PAD);
        }

        return row;
    }

    
    function create_row_state() {
        var row = new_row();
        var text = row.CreateFontString();
        text.SetPoint(LEFT);
        text.ExSetFont();
        text.SetText("State:");

        var open_btn = new TextButton("Open", frame);
        open_btn.SetPoint(LEFT, text, RIGHT, Style.PAD, 0);
        open_btn.SetScript("OnClick", ()->{
            filter.data.is_open = !filter.data.is_open;
            open_btn.set_color(filter.data.is_open ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            bag_redraw();
        });

        
        var hidden_btn = new TextButton("Hidden", frame);
        hidden_btn.SetPoint(LEFT, open_btn, RIGHT, Style.PAD, 0);
        hidden_btn.SetScript("OnClick", ()->{
            filter.data.is_hidden = !filter.data.is_hidden;
            hidden_btn.set_color(filter.data.is_hidden ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            bag_redraw();
        });

        
        inits.push(()->{
            open_btn.set_color(filter.data.is_open     ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            hidden_btn.set_color(filter.data.is_hidden ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });
    }

    function create_row_automation() {
        var row = new_row();

        var text = frame.CreateFontString();
        text.SetPoint(LEFT, row);
        text.SetFont(Style.FONT, Style.FONT_SIZE);
        text.SetText("Automation:");

        var auto_open = new TextButton("Open", frame);
        auto_open.SetPoint(LEFT, text, RIGHT, Style.PAD, 0);
        auto_open.SetScript("OnClick", ()->{
            filter.data.auto_open = !filter.data.auto_open;
            auto_open.set_color(filter.data.auto_open ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });

        var auto_close = new TextButton("Close", frame);
        auto_close.SetPoint(LEFT, auto_open, RIGHT, Style.PAD, 0);
        auto_close.SetScript("OnClick", ()->{
            filter.data.auto_close = !filter.data.auto_close;
            auto_close.set_color(filter.data.auto_close ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });

        var auto_hide = new TextButton("Hide", frame);
        auto_hide.SetPoint(LEFT, auto_close, RIGHT, Style.PAD, 0);
        auto_hide.SetScript("OnClick", ()->{
            filter.data.auto_hide = !filter.data.auto_hide;
            auto_hide.set_color(filter.data.auto_hide ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });

        inits.push(()->{
            auto_open .set_color(filter.data.auto_open   ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            auto_close.set_color(filter.data.auto_close  ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            auto_hide .set_color(filter.data.auto_hide   ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });
    }

    function create_row_name() {
        var row = new_row();
        /// NAME
        var name_text = row.CreateFontString();
        var name_save = new TextButton("Save", row);
        var name_edit = new TextEdit(frame);


        name_text.SetPoint(LEFT);
        name_text.SetFont(Style.FONT, Style.FONT_SIZE);
        name_text.SetText("Name:");


        name_save.SetPoint(RIGHT);
        name_save.SetScript("OnClick", ()->{
            name_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
            name_edit.ClearFocus();
            filter.data.name = name_edit.GetText();
            bag_update();
        });


        name_edit.SetPoint(LEFT, name_text, RIGHT, Style.PAD, 0);
        name_edit.SetPoint(RIGHT, name_save, LEFT, -Style.PAD, 0);
        name_edit.SetScript("OnEnterPressed", ()->{
            name_edit.ClearFocus();
            name_save.Click();
        });
        name_edit.SetScript("OnTextChanged", ()->{
            if (filter.data.name == name_edit.GetText()) {
                name_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
            } else {
                name_save.ExSetBackdropColor(Style.BUTTON_OK);
            }
        });

        inits.push(()->{
            name_edit.SetText(filter.data.name);
        });
    }

    function create_row_icon() {
        var row = new_row();
        /// ICON
        var icon_text = row.CreateFontString();
        var icon_save = new TextButton("Save", frame);
        var icon_pick = new TextButton("Pick", frame);
        var icon_edit = new TextEdit(frame);


        icon_text.SetPoint(LEFT);
        icon_text.SetFont(Style.FONT, Style.FONT_SIZE);
        icon_text.SetText("Icon:");

        icon_save.SetPoint(RIGHT, frame, RIGHT, -Style.PAD, 0);
        icon_save.SetPoint(TOP, icon_text, TOP, 0, 0);
        icon_save.SetPoint(BOTTOM, icon_text, BOTTOM, 0, 0);
        icon_save.SetScript("OnClick", ()->{
            icon_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
            icon_edit.ClearFocus();
            filter.data.icon = icon_edit.GetText();
            bag_redraw();
        });

        icon_pick.SetPoint(RIGHT, icon_save, LEFT, -Style.PAD, 0);
        icon_pick.SetScript("OnClick", ()->{
            IconPicker.INSTANCE.pick((icon)->{
                icon_edit.SetText(icon);
                icon_save.Click();
            });
        });

        icon_edit.SetPoint(LEFT, icon_text, RIGHT, Style.PAD, 0);
        icon_edit.SetPoint(RIGHT, icon_pick, LEFT, -Style.PAD, 0);
        icon_edit.SetScript("OnEnterPressed", ()->{
            icon_edit.ClearFocus();
            icon_save.Click();
        });
        icon_edit.SetScript("OnTextChanged", ()->{
            if (filter.data.icon == icon_edit.GetText()) {
                icon_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
            } else {
                icon_save.ExSetBackdropColor(Style.BUTTON_OK);
            }
        });

        inits.push(()->{
            icon_edit.SetText(filter.data.icon);
        });
    }

    function create_row_code() {
        var row = new_row();

        /// CODE
        var code_text = row.CreateFontString();
        code_text.SetPoint(LEFT);
        code_text.SetFont(Style.FONT, Style.FONT_SIZE);
        code_text.SetText("Code:");

        var volatile = new TextButton("Volatile", row);
        volatile.SetPoint(LEFT, code_text, RIGHT, Style.PAD, 0);
        volatile.SetScript("OnClick", ()->{
            filter.data.volatile = !filter.data.volatile;
            volatile.set_color(filter.data.volatile ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
        });

        var code_save = new TextButton("Save", frame);
        var msg_bg:Frame = wow.G.CreateFrame(FRAME, null, frame);
        var code_edit = new CodeEditBox(frame);

        var code_msg : FontString = msg_bg.CreateFontString();

        code_save.SetPoint(RIGHT, frame, RIGHT, -Style.PAD, 0);
        code_save.SetPoint(TOP, code_text, TOP, 0, 0);
        code_save.SetPoint(BOTTOM, code_text, BOTTOM, 0, 0);
        code_save.SetScript("OnClick", ()->{
            if (code_msg.GetText() == null) { 
                code_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
                code_edit.clear_focus();
                filter.set_code(code_edit.get_code());
                bag_redraw();
            }
        });
        
        msg_bg.SetFrameStrata(FrameStrata.TOOLTIP);
        msg_bg.SetBackdrop(Style.BACKDROP);
        msg_bg.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        msg_bg.SetToplevel(true);
        msg_bg.Hide();
        
        code_msg.SetPoint(LEFT, code_save, RIGHT, 3*Style.PAD, 0);
        code_msg.SetFont(Style.FONT, Style.FONT_SIZE);
        code_msg.SetWidth(250);
        code_msg.SetJustifyH(LEFT);
        msg_bg.SetPoint(TOPLEFT, code_msg, TOPLEFT, -Style.PAD, Style.PAD);
        msg_bg.SetPoint(BOTTOMRIGHT, code_msg, BOTTOMRIGHT, Style.PAD, -Style.PAD);

        
        code_save.SetScript("OnEnter", ()->{ 
            if (code_msg.GetText() != null) {
                msg_bg.Show(); 
            }
        });
        code_save.SetScript("OnLeave", ()->{ 
            msg_bg.Hide();
        });

        code_edit.SetPoint(TOPLEFT, code_text, BOTTOMLEFT, 0, -Style.PAD);
        code_edit.SetPoint(BOTTOMRIGHT, frame, BOTTOMRIGHT, -Style.PAD, Style.PAD);
        

        code_edit.on_code_changed(()->{
            code_msg.SetText(null);
            if (filter.data.code == code_edit.get_code()) {
                code_save.ExSetBackdropColor(Style.BUTTON_NORMAl);
            } else {
                var se = Filter.validate_code(code_edit.get_code());
                if (se == null) {
                    code_save.ExSetBackdropColor(Style.BUTTON_OK);
                } else {
                    code_msg.SetText('${se.type} Error on line ${se.line}\n${se.msg}');
                    code_save.ExSetBackdropColor(Style.BUTTON_FAIL);
                }
            }
        });

        inits.push(()->{
            volatile.set_color(filter.data.volatile ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            code_edit.set_code(filter.data.code);
            code_edit.set_cursor(0);
        });


    }

    function bag_redraw() {
        filter.bag.redraw();
        if (BagEdit.INSTANCE.bag == filter.bag) {
            BagEdit.INSTANCE.edit(filter.bag);
        }
    }

    function bag_update() {
        filter.bag.update();
        if (BagEdit.INSTANCE.bag == filter.bag) {
            BagEdit.INSTANCE.edit(filter.bag);
        }
    }

    public function edit(filter:Filter) {
        this.filter = filter;
        for (fn in inits) fn();
        frame.Show();
        open = true;
    }

    public function hide() {
        IconPicker.INSTANCE.hide();
        frame.Hide();
        open = false;
        filter = null;
    }

    public static final INSTANCE = new FilterEdit();
}