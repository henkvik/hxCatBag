package widget;

import wow.widget.GameTooltip;
import lua.Table.AnyTable;
import wow.widget.EditBox;
import wow.widget.Button;
import wow.widget.Frame;
using Style.StyleEx;

class BagEdit {
    static public final INSTANCE = new BagEdit();

    var open : Bool;

    public var bag : Bag;
    var frame : Frame;
    var content : Frame;
    var rows : Array<Frame>;

    var filters_row : Frame;
    var filter_rows : Array<Frame>;

    var bag_row : Frame;
    var bag_btns: Array<Button>;

    var max_edit : TextEdit;
    var max_indicator : Frame;
    var reverse_btn : TextButton;

    public function new() {
        rows = new Array<Frame>();
        filter_rows = new Array<Frame>();

        create_frame();

        create_bag_changer();
        create_max();
        create_reverse();
        create_filters();
    }

    function create_frame() {
        frame = wow.G.CreateFrame(FRAME, null, wow.G.UIParent);
        frame.SetSize(300, 200);
        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        frame.SetPoint(TOPLEFT, wow.G.UIParent, CENTER, -150, 100);
        frame.SetToplevel(true);
        frame.Hide();
        

        // make draggable
        frame.SetMovable(true);
        frame.EnableMouse(true);
        frame.RegisterForDrag(LeftButton);
        frame.SetScript("OnDragStart", frame.StartMoving);
        frame.SetScript("OnDragStop", ()->{
            frame.StopMovingOrSizing();
            frame.SetPoint(TOPLEFT, wow.G.UIParent, BOTTOMLEFT, frame.GetLeft(), frame.GetTop());
        });


        var title = frame.CreateFontString();
        title.SetPoint(TOP, frame, TOP, 0, -Style.PAD);
        title.SetFont(Style.FONT, Style.FONT_SIZE);
        title.SetText("Bag Settings");

        var close = new TextButton("X", frame);
        close.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);
        close.SetScript("OnClick",()->{
            hide();
            IconPicker.INSTANCE.hide();
            max_indicator.Hide();
        });


        // frame.SetResizable(true);
        // frame.SetMinResize(200, 250);
        // var resize:Button = wow.G.CreateFrame(BUTTON, null, frame);
        // resize.SetSize(16,16);
        // resize.SetPoint(BOTTOMRIGHT);
        // resize.SetNormalTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Up");
        // resize.SetPushedTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Down");
        // resize.SetHighlightTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Highlight");

        // resize.RegisterForClicks(LeftButtonUp);
        // resize.SetScript("OnMouseDown", ()-> { 
        //     frame.StartSizing(BOTTOMRIGHT);
        //     resize.GetHighlightTexture().Hide();
        // });
        // resize.SetScript("OnMouseUp", ()->{
        //     frame.StopMovingOrSizing();
        //     resize.GetHighlightTexture().Show();
        // });

        content = wow.G.CreateFrame(FRAME, null, frame);
        content.SetPoint(TOP, title, BOTTOM, 0, -Style.PAD);
        content.SetPoint(LEFT, frame, LEFT, Style.PAD, 0);
        content.SetPoint(RIGHT, frame, RIGHT, -Style.PAD, 0);
        content.SetPoint(BOTTOM, frame, BOTTOM, 0, Style.PAD);




        var g:AnyTable = untyped __lua__("_G");
        g.ContainerFrame1.HookScript("OnHide", () -> {hide();});
    }

    function create_bag_changer() {
        bag_row = new_row();
        bag_row.SetHeight(Style.SLOT_SIZE);
        bag_btns = [];
    }

    function create_bag(bag_id:Int) {
        if (bag_id <= 0) return;

        var btn:Button = wow.G.CreateFrame(BUTTON, null, bag_row);
        var i = bag_btns.push(btn) - 1;

        btn.SetSize(Style.SLOT_SIZE, Style.SLOT_SIZE);

        var inv_id = wow.Inventory.ContainerIDToInventoryID(bag_id);
        var id = wow.Inventory.GetInventoryItemID("player", inv_id);
        var icon = wow.Item.GetItemIcon(id);

        if (icon == null) {
            var sinfo = wow.Inventory.GetInventorySlotInfo("Bag0Slot");
            btn.SetNormalTexture(sinfo.texture);
            btn.GetNormalTexture().ExSetTexCoordInset(Style.ICON_INSET);
        } else {
            btn.SetNormalTexture(icon);
            btn.SetHighlightTexture(icon);
        }

        if (i == 0) {
            btn.SetPoint(LEFT);
        } else {
            btn.SetPoint(LEFT, bag_btns[i-1], RIGHT, Style.SLOT_PAD, 0);
        }

        btn.RegisterForClicks(LeftButtonUp);
        btn.RegisterForDrag(LeftButton);
        btn.SetScript("OnClick", ()-> {
            if (wow.Cursor.CursorHasItem()) {
                wow.Bag.PutItemInBag(inv_id);
            } else {
                wow.Inventory.PickupBagFromSlot(inv_id);
            }
        });
        btn.SetScript("OnDragStart", ()->btn.Click());
        btn.SetScript("OnReceiveDrag", ()->btn.Click());

        btn.RegisterEvent("ITEM_UNLOCKED");
        btn.SetScript("OnEvent", (_,e,bid)->{
            if (bid == inv_id) {
                bag.update();
                edit(bag);
            }
        });

        btn.SetScript("OnEnter", ()->{
            wow.G.GameTooltip.SetOwner(btn, ANCHOR_LEFT, -Style.SLOT_PAD,Style.SLOT_PAD);
            wow.G.GameTooltip.SetInventoryItem("player", inv_id);
        });

        btn.SetScript("OnLeave", ()->{
            wow.G.GameTooltip.ClearLines();
            wow.G.GameTooltip.Hide();
        });
    }

    function create_max() {
        var row = new_row();

        
        var max_text = row.CreateFontString();
        var max_save = new TextButton("Save", row);
        var max_inc = new TextButton("+", row);
        var max_dec = new TextButton("-", row);
        max_edit = new TextEdit(row);
        max_indicator = wow.G.CreateFrame(FRAME, null, row);

        max_text.SetPoint(LEFT);
        max_text.ExSetFont();
        max_text.SetText("Max Height:");

        max_edit.SetPoint(LEFT, max_text, RIGHT, Style.PAD, 0);
        max_edit.SetWidth(Style.FONT_SIZE*4);
        max_edit.SetNumeric(true);
        max_edit.SetJustifyH(CENTER);
        max_edit.SetScript("OnTextChanged", ()->{
            max_edit.SetNumber(max_edit.GetNumber());
            if (max_edit.GetNumber() == bag.data.max_height) {
                max_save.set_color(Style.BUTTON_NORMAl);
            } else {
                max_save.set_color(Style.BUTTON_OK);
            }

            max_indicator.SetPoint(BOTTOMRIGHT, bag.frame, BOTTOMRIGHT, 0, max_edit.GetNumber());
        });
        max_edit.RegisterForDrag(LeftButton);
        
        max_save.SetPoint(RIGHT, row);
        max_save.SetScript("OnClick",()->{
            max_edit.ClearFocus();
            max_save.set_color(Style.BUTTON_NORMAl);
            bag.data.max_height = max_edit.GetNumber();
            bag.redraw();
        });

        max_dec.SetPoint(RIGHT, max_save, LEFT, -Style.PAD, 0);
        max_dec.SetScript("OnClick", ()->{
            max_edit.SetNumber(max_edit.GetNumber() - 50);
            max_save.Click();
        });

        max_inc.SetPoint(RIGHT, max_dec, LEFT, -Style.PAD, 0);
        max_inc.SetScript("OnClick", ()->{
            max_edit.SetNumber(max_edit.GetNumber() + 50);
            max_save.Click();
        });


        max_indicator.SetBackdrop(Style.BACKDROP);
        max_indicator.SetBackdropColor(0.3,1,0.3,0.5);
        max_indicator.SetSize(100, 2);
        max_indicator.SetFrameStrata(FrameStrata.DIALOG);
        max_indicator.Hide();

        var ind_text = max_indicator.CreateFontString();
        ind_text.SetPoint(BOTTOMRIGHT, max_indicator, TOPRIGHT);
        ind_text.SetFont(Style.FONT, Style.FONT_SIZE);
        ind_text.SetText("Max Height");
    }

    function create_reverse() {
        var row = new_row();

        var text = row.CreateFontString();
        text.SetPoint(LEFT);
        text.ExSetFont();
        text.SetText("Draw Order:");

        reverse_btn = new TextButton("Reverse", row);
        reverse_btn.SetPoint(LEFT, text, RIGHT, Style.PAD, 0);
        reverse_btn.SetScript("OnClick", ()->{
            bag.data.reverse = !bag.data.reverse;
            reverse_btn.set_color(bag.data.reverse ? Style.BUTTON_OK : Style.BUTTON_NORMAl);
            bag.redraw();
        });
    }

    function create_filters() {
        var row = new_row();
        filters_row = new_row();

        var text = row.CreateFontString();
        text.SetPoint(LEFT);
        text.ExSetFont();
        text.SetText("Categories:");

        var new_btn = new TextButton("New", row);
        new_btn.SetPoint(TOPRIGHT);
        new_btn.SetScript("OnClick", ()->{
            bag.add_filter(new Filter(
                "New Filter",
                "Interface/PaperDollInfoFrame/UI-GearManager-ItemIntoBag",
                "return function(item)\n  return not item.empty;\nend"
            ));
            bag.update();
            edit(bag);
        });
    }

    function sort_filters() {
        for (i in 0...filter_rows.length) {
            filter_rows[i].SetID(i);
            filter_rows[i].SetPoint(TOPLEFT, filters_row, TOPLEFT, 0, 
                -(Style.PAD +Style.FONT_SIZE) * i
            );
        }
    }

    function move_up_filter(row : Frame) {
        var i = filter_rows.indexOf(row);
        if (i > 0) {
            var tmp = filter_rows[i];
            filter_rows[i] = filter_rows[i-1];
            filter_rows[i-1] = tmp;
        }
    }

    function move_down_filter(row : Frame) {
        var i = filter_rows.indexOf(row);
        if (i >= 0 && i < filter_rows.length-1) {
            var tmp = filter_rows[i];
            filter_rows[i] = filter_rows[i+1];
            filter_rows[i+1] = tmp;
        }
    }

    function create_filter(filter:Filter) {
        var row:Frame = wow.G.CreateFrame(FRAME, null, filters_row);
        row.SetHeight(Style.FONT_SIZE);
        row.SetPoint(RIGHT);
        row.Show();

        var row_id = filter_rows.push(row) - 1;
        row.SetID(row_id);
        row.SetPoint(TOPLEFT, filters_row, TOPLEFT, 0, 
            -(Style.PAD +Style.FONT_SIZE) * row_id
        );

        filters_row.SetHeight((Style.FONT_SIZE + Style.PAD) * (filter_rows.length) - Style.PAD);

        row.SetMovable(true);
        row.SetScript("OnMouseDown", ()->{
            var scale = wow.G.UIParent.GetScale();
            var height = row.GetHeight();
            var last_id = filter_rows.length - 1;
            row.SetScript("OnUpdate", ()->{
                var id = row.GetID();
                var delta = wow.G.GetCursorPosition().y - row.GetCenter().y * scale;
                var diff = Math.floor((delta + height * 0.5) / height);

                if (diff > 0 && id != 0) {
                    bag.move_up_filter(filter);
                    move_up_filter(row);
                    sort_filters();
                };

                if (diff < 0 && id != last_id) {
                    bag.move_down_filter(filter);
                    move_down_filter(row);
                    sort_filters();
                };
            });
        });
        row.SetScript("OnMouseUp", ()->{
            row.SetScript("OnUpdate",null);
            bag.update();
        });

        var icon = row.CreateTexture();
        icon.SetSize(Style.FONT_SIZE, Style.FONT_SIZE);
        icon.SetPoint(LEFT);
        icon.SetTexture(filter.data.icon);
        icon.ExSetTexCoordInset(Style.ICON_INSET);

        var title = row.CreateFontString();
        title.ExSetFont();
        title.SetPoint(LEFT, icon, RIGHT, Style.PAD, 0);
        title.SetText(filter.data.name);
        title.SetTextColor(1,1,1,filter.data.is_hidden ? 0.75 : 1);

        var delete = new TextButton("X", row);
        delete.SetPoint(RIGHT);
        delete.SetScript("OnLeave",()-> {
            delete.SetID(0);
            delete.set_color(Style.BUTTON_NORMAl);
        });
        delete.SetScript("OnClick", ()->{
            if (delete.GetID() == 1) {
                bag.remove_filter(filter);
                bag.update();
                edit(bag);
            } else {
                delete.SetID(1);
                delete.set_color(Style.BUTTON_FAIL);
            }
        });

        var edit_btn = new TextButton("Edit", row);
        edit_btn.SetPoint(RIGHT, delete, LEFT, -Style.PAD, 0);
        edit_btn.SetScript("OnClick", ()->{
            FilterEdit.INSTANCE.edit(filter);
        });


    }


    function new_row() {
        var row:Frame = wow.G.CreateFrame(FRAME, null, content);
        row.SetHeight(Style.FONT_SIZE);
        row.SetPoint(RIGHT);
        row.Show();

        var row_id = rows.push(row) - 1;
        if (row_id == 0) {
            row.SetPoint(TOPLEFT);
        } else {
            row.SetPoint(TOPLEFT, rows[row_id-1], BOTTOMLEFT, 0, -Style.PAD);
        }

        return row;
    }

    public function edit(bag:Bag) {
        while (bag_btns.length > 0) {
            var btn = bag_btns.pop();
            btn.UnregisterAllEvents();
            btn.Hide();
        };
        for (bid in bag.bag_ids) { create_bag(bid); }


        while (filter_rows.length > 0) {
            var row = filter_rows.pop();
            row.SetScript("OnUpdate",null);
            row.Hide();
        };
        filters_row.SetHeight(0.0001);

        frame.Show();
        this.bag = bag;

        max_edit.SetNumber(bag.data.max_height);
        reverse_btn.set_color(bag.data.reverse ? Style.BUTTON_OK : Style.BUTTON_NORMAl);

        for (filter in bag.filters) {
            create_filter(filter);
        }

        max_indicator.Show();

        var rh = 0.0; 
        for (row in rows) rh += row.GetHeight() + Style.PAD;
        frame.SetHeight(Style.PAD*2 + Style.FONT_SIZE + rh);

        open = true;
    }

    public function hide() {
        open = false;
        frame.Hide();
        bag = null;

        while (bag_btns.length > 0) {
            var btn = bag_btns.pop();
            btn.UnregisterAllEvents();
        };
    }

    public function is_shown() return frame.IsShown();
}