package widget;

import wow.G.FloatXY;
import wow.widget.Frame;

using Style.StyleEx;

class Window {
    var frame : Frame;
    var rows : Array<Frame>;

    public function new(title:String, width:Float, height:Float) {
        rows = new Array<Frame>();

        frame = wow.G.CreateFrame(FRAME, null, wow.G.UIParent);
        frame.SetSize(width, height);
        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        frame.SetPoint(CENTER, wow.G.UIParent, CENTER);
        frame.SetToplevel(true);
        frame.Hide();

        // make draggable
        frame.SetMovable(true);
        frame.EnableMouse(true);
        frame.RegisterForDrag(LeftButton);
        frame.SetScript("OnDragStart", frame.StartMoving);
        frame.SetScript("OnDragStop", ()->{
            frame.StopMovingOrSizing();
            frame.SetPoint(TOP, wow.G.UIParent, BOTTOMLEFT, frame.GetCenter().x, frame.GetTop());
        });

        var title_fs = frame.CreateFontString();
        title_fs.SetPoint(TOP, frame, TOP, 0, -Style.PAD);
        title_fs.SetFont(Style.FONT, Style.FONT_SIZE);
        title_fs.SetText(title);

        var close = new TextButton("X", frame);
        close.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);
        close.SetScript("OnClick",()->{
            frame.Hide();
        });
    }

    public function show() frame.Show();
    public function hide() frame.Hide();

    // public function set_resizeable() {
        // frame.SetResizable(true);
        // frame.SetMinResize(200, 250);
        // var resize:Button = wow.G.CreateFrame(BUTTON, null, frame);
        // resize.SetSize(16,16);
        // resize.SetPoint(BOTTOMRIGHT);
        // resize.SetNormalTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Up");
        // resize.SetPushedTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Down");
        // resize.SetHighlightTexture("Interface/CHATFRAME/UI-ChatIM-SizeGrabber-Highlight");

        // resize.RegisterForClicks(LeftButtonUp);
        // resize.SetScript("OnMouseDown", ()-> { 
        //     frame.StartSizing(BOTTOMRIGHT);
        //     resize.GetHighlightTexture().Hide();
        // });
        // resize.SetScript("OnMouseUp", ()->{
        //     frame.StopMovingOrSizing();
        //     resize.GetHighlightTexture().Show();
        // });
    // }

    public function new_row() {
        var row:Frame = wow.G.CreateFrame(FRAME, null, frame);
        row.SetPoint(RIGHT, frame, RIGHT, -Style.PAD, 0);
        row.SetHeight(Style.FONT_SIZE);
        row.Show();

        var row_id = rows.push(row) - 1;
        if (row_id == 0) {
            row.SetPoint(TOPLEFT, frame, TOPLEFT, Style.PAD, -(Style.PAD*2+Style.FONT_SIZE));
        } else {
            row.SetPoint(TOPLEFT, rows[row_id-1], BOTTOMLEFT, 0, -Style.PAD);
        }

        return row;
    }

    public function update_height() {
        
        var height = Style.PAD;//* 2 + Style.FONT_SIZE;
        height += frame.GetTop() - rows[rows.length-1].GetBottom();

        // for (row in rows) {
        //     height += Style.PAD * row.GetHeight();
        // }


        frame.SetHeight(height);
    }
}