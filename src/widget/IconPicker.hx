package widget;

import wow.widget.Texture.TexturePath;
import wow.widget.Texture.AlphaMode;
import wow.widget.Button;
import wow.widget.FontString;
import wow.widget.Frame;

using Style.StyleEx;

class IconPicker {
    static public final INSTANCE = new IconPicker();

    var offset : Int;

    var frame : Frame;
    var title : FontString;
    var close : TextButton;
    var icons : Array<Icon>;

    var on_pick : TexturePath->Void;

    public function new() {
        offset = 1;
        on_pick = (i)->{trace(i);};

        var icon_size = Style.ICON_SIZE;

        frame = wow.G.CreateFrame(FRAME, null, wow.G.UIParent);
        frame.SetSize(
            Style.PICKER_COLS * (icon_size + Style.PAD) + Style.PAD * 1,
            Style.PICKER_ROWS * (icon_size + Style.PAD) + Style.PAD * 2 + Style.FONT_SIZE
        );

        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        frame.SetPoint(CENTER, wow.G.UIParent, CENTER);
        frame.SetToplevel(true);
        frame.Hide();
        
        // make draggable
        frame.SetMovable(true);
        frame.EnableMouse(true);
        frame.RegisterForDrag(LeftButton);
        frame.SetScript("OnDragStart", frame.StartMoving);
        frame.SetScript("OnDragStop", frame.StopMovingOrSizing);

        // enable scrolling
        frame.EnableMouseWheel(true);
        frame.SetScript("OnMouseWheel", (_, d)->{
            offset -= d * Style.PICKER_ROWS*Style.PICKER_ROWS;

            offset = cast Math.max(offset, 1);
            offset = cast Math.min(offset, wow.Addon.table.ICONS.length - Style.PICKER_COLS*Style.PICKER_ROWS);
            redraw();
        });


        title = frame.CreateFontString(null, ARTWORK);
        title.SetFont(Style.FONT, Style.FONT_SIZE);
        title.SetPoint(TOP, frame, TOP, 0, -Style.PAD);
        title.SetText("Icon Picker");

        close = new TextButton("X", frame);
        close.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);
        close.SetScript("OnClick", ()->{
            frame.Hide();
        });

        icons = new Array<Icon>();
        for (i in 0... Style.PICKER_COLS*Style.PICKER_ROWS) {
            var icon = new Icon(icon_size, frame, 
                (i % Style.PICKER_COLS) * (icon_size + Style.PAD) + Style.PAD,
                -Math.floor(i / Style.PICKER_COLS) * (icon_size + Style.PAD)
                -Style.PAD * 2 - Style.FONT_SIZE
            );
            icon.set_icon(i+offset);
            icon.SetID(i);
            icon.SetScript("OnClick", ()->{
                on_pick(icon.GetNormalTexture().GetTexture());
            });

            icons[i] = icon;
        }
    }

    public function pick(on_pick :TexturePath->Void) {
        this.on_pick = on_pick;
        frame.Show();
    }

    public function hide() {
        frame.Hide();
    }

    public function redraw() {
        for (icon in icons) {
            icon.set_icon(offset + icon.GetID());
        }
    }
    
}

@:forward
abstract Icon(Button) {
    public function new(size:Float, parent :Frame, x:Float, y:Float) {
        this = wow.G.CreateFrame(BUTTON, null, parent);
        this.SetSize(size, size);
        this.SetPoint(TOPLEFT, parent, TOPLEFT, x, y);

        this.SetNormalTexture("Interface\\BUTTONS\\UI-AutoCastableOverlay");
        this.GetNormalTexture().ExSetTexCoordInset(Style.ICON_INSET);
        this.SetHighlightTexture("Interface\\BUTTONS\\UI-AutoCastableOverlay", AlphaMode.BLEND);
        this.GetHighlightTexture().SetTexCoord(
            14/64, 1-15/64,
            14/64, 1-15/64
        );
    }

    public function set_icon(offset:Int) {
        this.SetNormalTexture(wow.Addon.table.ICONS[offset]);
    }
}