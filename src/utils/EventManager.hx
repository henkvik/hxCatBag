package utils;

import haxe.ds.Map;
import haxe.Constraints.Function;
import wow.widget.Frame;

class EventManager {
    var frame : Frame;
    var callbacks : Map<String, Function>;

    public function new() {
        callbacks = new Map();
        frame = wow.G.CreateFrame(FRAME);
        frame.SetScript("OnEvent", untyped __lua__("function(_, e, ...) {0}.h[e](...); end", callbacks));
    }

    public function register(event: String, callback:Function) {
        frame.RegisterEvent(event);
        callbacks.set(event, callback);
    }

    public function unregister(event:String) {
        frame.UnregisterEvent(event);
        callbacks.remove(event);
    }
}