package utils;

import wow.widget.FontString;
import wow.widget.Region;

typedef ItemScan = { tooltip:String, has_red:Bool};

class TooltipScanner {

    static var scanner : wow.widget.GameTooltip;
    static function __init__() {
        // untyped __lua__("
        // CreateFrame( \"GameTooltip\", \"MyScanningTooltip\" ); -- Tooltip name cannot be nil
        // MyScanningTooltip:SetOwner( WorldFrame, \"ANCHOR_NONE\" );
        // -- Allow tooltip SetX() methods to dynamically add new lines based on these
        // MyScanningTooltip:AddFontStrings(
        //     MyScanningTooltip:CreateFontString( \"$parentTextLeft1\", nil, \"GameTooltipText\" ),
        //     MyScanningTooltip:CreateFontString( \"$parentTextRight1\", nil, \"GameTooltipText\" )
        // );
        // ");
        // tooltip = untyped __lua__("_G[\"MyScanningTooltip\"]");

        scanner = wow.G.CreateFrame(GAMETOOLTIP, "CATBAGSCANNERTOOLTIP");
        scanner.SetOwner(untyped __lua__("WorldFrame"), ANCHOR_NONE);
        scanner.AddFontStrings(
            scanner.CreateFontString("$parentTextLeft1", null, "GameTooltipText"),
            scanner.CreateFontString("$parentTextRight1", null, "GameTooltipText")
        );
    }

    public static function scan_bag_slot_tooltip(bag:Int,slot:Int) : ItemScan {
        scanner.ClearLines();
        scanner.SetBagItem(bag, slot);
        
        // Add support for Pawn.
        var pwn : Dynamic = untyped __lua__("PawnUpdateTooltip");
        if (pwn) pwn(scanner.GetName(), "SetBagItem", bag, slot);

        var tooltip = "";
        var has_red = false;
        untyped __lua__("
        for i=1,select(\"#\",{0}:GetRegions()) do
            local region=select(i,{0}:GetRegions())
            if region and region:GetObjectType()==\"FontString\" and region:GetText() then
                {1} = {1} .. region:GetText() .. \"\\n\"
                local r, g, b = region:GetTextColor();
                if (r > 0.99 and g < 0.13 and b < 0.13) then
                    {2} = true;
                end
            end
         end
        ", scanner, tooltip, has_red);
        return { tooltip:tooltip, has_red:has_red };
    }
}

