
import haxe.ds.Map;
import wow.widget.FontString;
import lua.Lua.CollectGarbageOption;
import widget.FilterEdit;
import lua.Bit;
import wow.Cursor;
import widget.BagEdit;
import haxe.macro.Format;
import lua.Table.AnyTable;
import wow.widget.Font;
import wow.widget.Button;
import wow.Bag.BagType;
import wow.widget.Frame;
import Filter.FilterData;

using Style.StyleEx;


class BagData {
    public var filters    : Array<FilterData> = [];
    public var columns    : Int = 5;
    public var max_height : Float = 700;
    public var reverse    : Bool = false;
    public var x          : Float = null;
    public var y          : Float = null;
    public function new() {}
}

class Bag {
    public var frame(default, null) : Frame;
    var slots : Array<Slot>;
    var gold : Button;

    var empty_count : FontString;

    public var filters(default,null) : Array<Filter>;

    public var data : BagData;

    public var bag_ids = [0, 1, 2, 3, 4];
    var bag_slot_cache:Map<Int, Map<Int, Slot>>;


    public function new() {
        bag_slot_cache = new Map();
        for (id in bag_ids) {
            bag_slot_cache.set(id, new Map());
        }

        filters = new Array<Filter>();
        slots = new Array<Slot>();

        data = new BagData();

        create_frame();
        create_empty_count();
        create_gold_count();
    }

    public function show() {
        frame.Show();
    }

    public function hide() {
        frame.Hide();
    }

    public function is_shown() {
        return frame.IsShown();
    }

    public function add_filter(filter : Filter) {
        filters.push(filter);
        filter.bag = this;
    }

    public function remove_filter(filter : Filter) {
        if (filters.remove(filter)) {
            filter.hide();
            if (FilterEdit.INSTANCE.filter == filter) {
                FilterEdit.INSTANCE.hide();
            }
        }
    }

    public function move_up_filter(filter : Filter) {
        var i = filters.indexOf(filter);
        if (i > 0) {
            var tmp = filters[i];
            filters[i] = filters[i-1];
            filters[i-1] = tmp;
        }
    }

    public function move_down_filter(filter : Filter) {
        var i = filters.indexOf(filter);
        if (i >= 0 && i < filters.length-1) {
            var tmp = filters[i];
            filters[i] = filters[i+1];
            filters[i+1] = tmp;
        }
    }

    public function update() {
        if (wow.G.InCombatLockdown()) return;

        query_slots();

        for (slot in slots) {
            slot.update();
        }

        filter_slots(slots);

        redraw();
        
        lua.Lua.collectgarbage(CollectGarbageOption.Collect);
    }

    public function filter_slots(slots : Array<Slot>) {
        var u_slots = slots;
        for (filter in filters) {
            u_slots = filter.filter_slots(u_slots);
        }

        for (slot in u_slots) {
            slot.hide();
        }
    }

    public function redraw() {
        if (wow.G.InCombatLockdown()) return;

        frame.SetWidth(Style.calc_width(data.columns));
        if (gold != null) gold.SetShown(frame.GetWidth() > 120);
        update_empty_count();

        redraw_open();
        redraw_closed();
    }

    function redraw_open() {
        var x = 0.0;
        var y = Style.PAD + Style.calc_height(0);
        var first = true;
        for (filter in new ArrayIterator(filters, !data.reverse)) {
            if (filter.data.is_open) {
                filter.redraw(data.columns);
                if (filter.data.is_hidden) continue;

                if (!first && (y + filter.get_height()) > data.max_height) {
                    y = 0;
                    x -= Style.PAD + Style.calc_width(data.columns);
                }

                filter.anchor(BOTTOMLEFT, frame, BOTTOMLEFT, x, y);
                y += Style.PAD + filter.get_height();
                first = false;
            }
        }
    }

    function redraw_closed() {
        var wrap = frame.GetWidth();
        var x = 0.0;
        var y = -Style.PAD;
        for (filter in filters) {
            if (!filter.data.is_open) {
                filter.redraw(data.columns);
                if (filter.data.is_hidden) continue;

                if ((x + filter.get_height()) > wrap) {
                    x = 0;
                    y -= Style.PAD + filter.get_height();
                }

                filter.anchor(TOPLEFT, frame, BOTTOMLEFT, x, y);
                x += Style.PAD + filter.get_height();
            }
        }
    }

    function create_frame() {

        frame = wow.G.CreateFrame(FRAME, null, wow.G.UIParent);

        frame.SetSize(Style.calc_width(data.columns), Style.calc_height(0));
        frame.SetBackdrop(new Backdrop("Interface\\BUTTONS\\WHITE8X8"));
        frame.ExSetBackdropColor(Style.BACKGROUND_COLOR);
        frame.SetPoint(BOTTOMLEFT, wow.G.UIParent, BOTTOMRIGHT, -400, 200);
        frame.SetFrameStrata(FrameStrata.HIGH);
        frame.Hide();

        // Make Dragable
        frame.SetMovable(true);
        frame.EnableMouse(true);
        frame.RegisterForDrag(LeftButton);
        frame.SetScript("OnDragStart", frame.StartMoving);
        frame.SetScript("OnDragStop", ()->{
            frame.StopMovingOrSizing();
            data.x = frame.GetLeft();
            data.y = frame.GetBottom();
            frame.ClearAllPoints();
            frame.SetPoint(BOTTOMLEFT, wow.G.UIParent, BOTTOMLEFT, data.x, data.y);
        });
    }

    function update_empty_count() {
        empty_count.SetText(cast calc_num_empty());
    }

    function create_empty_count() {
        var empty:Button = wow.G.CreateFrame(BUTTON, null, frame);
        empty.SetSize(Style.ICON_SIZE, Style.ICON_SIZE);
        empty.SetPoint(TOPRIGHT, frame, TOPRIGHT, -Style.PAD, -Style.PAD);

        empty.SetNormalTexture("Interface\\Buttons\\Button-Backpack-Up");
        empty.GetNormalTexture().ExSetTexCoordInset(Style.ICON_INSET);
        empty.SetHighlightTexture("Interface\\Buttons\\Button-Backpack-Up");
        empty.GetHighlightTexture().ExSetTexCoordInset(Style.ICON_INSET);

        empty.RegisterForClicks(LeftButtonUp, RightButtonUp);
        empty.SetScript("OnClick", (_, btn)->{
            if (Cursor.CursorHasItem()) return place_in_empty();
            if (btn == LeftButton) update();
            if (btn == RightButton) widget.BagEdit.INSTANCE.edit(this);
        });

        empty_count = empty.CreateFontString();
        empty_count.SetFont(Style.FONT, Style.FONT_SIZE);
        empty_count.ClearAllPoints();
        empty_count.SetPoint(RIGHT, empty, LEFT, -Style.PAD, 0);

        empty.RegisterForDrag(LeftButton);
        empty.SetScript("OnDragStart", ()->{
            var scale = wow.G.UIParent.GetScale();

            empty.SetScript("OnUpdate", ()->{
                var center = empty.GetCenter();
                var c_pos = wow.G.GetCursorPosition();
                var delta = {
                    x :c_pos.x - center.x * scale
                };
                var x_diff = Math.floor((delta.x + Style.SLOT_SIZE/2) / Style.SLOT_SIZE);


                if (x_diff != 0) {
                    data.columns = cast Math.max(2, data.columns + x_diff);
                    redraw();
                }                
            });
        });

        empty.SetScript("OnDragStop", ()->{
            empty.SetScript("OnUpdate", null);
        });

        empty.SetScript("OnReceiveDrag", ()->{
            if (Cursor.CursorHasItem()) return place_in_empty();
        });


    }

    function create_gold_count() {
        gold = wow.G.CreateFrame(BUTTON, null, frame);
        gold.SetSize(Style.ICON_SIZE, Style.ICON_SIZE);
        gold.SetPoint(TOPLEFT, frame, TOPLEFT, Style.PAD, -Style.PAD);
        gold.SetNormalTexture("Interface/MINIMAP/TRACKING/Auctioneer");
        gold.GetNormalTexture().ExSetTexCoordInset(Style.ICON_INSET);

        var fs = gold.CreateFontString();
        fs.SetFont(Style.FONT, Style.FONT_SIZE);
        fs.ClearAllPoints();
        fs.SetPoint(LEFT, gold, RIGHT, Style.PAD, 0);

        gold.RegisterEvent("PLAYER_MONEY");
        gold.RegisterEvent("PLAYER_ENTERING_WORLD");
        gold.SetScript("OnEvent", ()->{
            fs.SetText(cast format_currency(wow.G.GetMoney() / 10000));
        });
    }

    function format_currency(amount :Float) {
        if (amount < 10) return set_precision(amount, 2) + "";
        if (amount < 100) return set_precision(amount, 1) + "";
        if (amount < 1000) return set_precision(amount, 0) + "";
        amount /= 1000;
        if (amount < 10) return set_precision(amount, 2) + "k";
        if (amount < 100) return set_precision(amount, 1) + "k";
        if (amount < 1000) return set_precision(amount, 0) + "k";
        amount /= 1000;
        return set_precision(amount, 2) + "M";
    }

    function set_precision(value:Float, precision:Int) {
        var exp = Math.pow(10, precision);
        return Math.floor(value * exp) / exp;
    }

    public function place_in_empty() {
        if (!wow.Cursor.CursorHasItem()) return;
        for (slot in slots) if (slot.is_empty()) {
            slot.click();
            break;
        }
    }

    public function load(save:BagData) {
        if (save.columns    != null)    data.columns = save.columns;
        if (save.max_height != null) data.max_height = save.max_height;
        if (save.reverse    != null)    data.reverse = save.reverse;
        if (save.x          != null)          data.x = save.x;
        if (save.y          != null)          data.y = save.y;
        
        if (save.x != null && save.y != null) {
            frame.SetPoint(BOTTOMLEFT, wow.G.UIParent, BOTTOMLEFT, save.x, save.y);
        }

        while (filters.length > 0) remove_filter(filters[filters.length-1]);
        for (filter in save.filters) {
            add_filter(Filter.load(filter));
        }
    }

    public function save() : BagData {
        data.filters = filters.map(f->f.data);
        return data;
    }

    function query_slots() {
        while (slots.length > 0) {
            var slot = slots.pop();
            slot.hide();
        }

        for (bag_id in bag_ids) {
            var bag_cache = bag_slot_cache.get(bag_id);
            var num_slots = wow.Bag.GetContainerNumSlots(bag_id);
            for (slot_id in 1...num_slots+1) {
                var slot = bag_cache.get(slot_id);
                if (slot == null) {
                    slot = new Slot(this, bag_id, slot_id);
                    bag_cache.set(slot_id, slot);
                }

                slots.push(slot);
            }
        }
    }

    function calc_num_slots() {
        var num_slots = 0;
        for (bag in bag_ids) {
            num_slots += wow.Bag.GetContainerNumSlots(bag);
        }
        return num_slots;
    }

    function calc_num_empty() {
        var num_empty = 0;
        for (bag in bag_ids) {
            var free = wow.Bag.GetContainerNumFreeSlots(bag);
            num_empty += free.type == NORMAL ? free.num : 0;
        }
        return num_empty;
    }
}


class ArrayIterator<T> {
    var i : Int;
    var a : Array<T>;
    var r : Bool;

    public function new(array :Array<T>, ?reverse:Bool) {
        r = reverse;
        a = array;  
        i = r ? a.length-1 : 0;
    }

    public function hasNext() return r ? i >= 0 : i < a.length;
    public function next() return r ? a[i--] : a[i++];
}