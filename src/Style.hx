
import wow.widget.FontString;
import wow.widget.Frame.Backdrop;

class Style {
    public static var TEXTURE_SOLID = "Interface\\BUTTONS\\WHITE8X8";
    
    public static var BACKDROP = new Backdrop("Interface\\BUTTONS\\WHITE8X8");
    public static var BACKGROUND_COLOR = new Color(0,0,0,0.7);
    public static var PAD       = 5.0;

    public static var SLOT_PAD  = 2.0;
    public static var SLOT_SIZE = 37.0;

    public static var ICON_SIZE = 20.0;
    public static var ICON_INSET = 5/64;

    public static var FONT      = "Fonts\\FRIZQT__.TTF";
    public static var CODE_FONT = "Interface\\AddOns\\CatBag\\FiraCode-Medium.ttf";
    public static var FONT_SIZE = 13.0;

    public static var PICKER_COLS = 16;
    public static var PICKER_ROWS = 8;

    public static var BUTTON_OK = new Color(0.3, 1, 0.3, 0.2);
    public static var BUTTON_FAIL = new Color(1, 0.3, 0.3, 0.2);
    public static var BUTTON_NORMAl = new Color(1, 1, 1, 0.2);

    public static var EDIT_BACKGROUND = new Color(0.3, 0.7, 1, 0.2);
    public static var COLOR_STRING = new Color(206/255, 145/255, 120/255, 1);
    public static var COLOR_NUMBER = new Color(181/255, 206/255, 168/255, 1);
    public static var COLOR_BOOL   = new Color(86/255, 156/255, 214/255, 1);

    public static function calc_width(columns :Int) {
        return PAD * 2
             + columns * (SLOT_SIZE + SLOT_PAD)
             - SLOT_PAD;
    }

    public static function calc_height(rows:Int) {
        return PAD * 2
             + ICON_SIZE
             + rows * (SLOT_SIZE + SLOT_PAD)
             - (rows == 0 ? 0 : SLOT_PAD - PAD);
    } 
}

class Color {
    public var r:Float;
    public var g:Float;
    public var b:Float;
    public var a:Float;

    public function new(r:Float, g:Float, b:Float, a:Float) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
}

class StyleEx {
    public static function ExSetBackdropColor(frame:wow.widget.Frame, color:Color) {
        frame.SetBackdropColor(color.r, color.g, color.b, color.a);
    }

    public static function ExSetTextColor(frame:wow.widget.FontString, color:Color) {
        frame.SetTextColor(color.r, color.g, color.b, color.a);
    }

    public static function ExSetTexCoordInset(texture:wow.widget.Texture, inset:Float) {
        texture.SetTexCoord(inset, 1-inset, inset, 1-inset);
    }

    public static function ExSetFont(instance:FontString) {
        instance.SetFont(Style.FONT, Style.FONT_SIZE);
    }
}