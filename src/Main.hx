import wow.widget.Slider.SilderScript;
import utils.EventManager;
import lua.Table.AnyTable;
import wow.SavedVariable;
import wow.Addon;
import widget.BagEdit;
import wow.widget.Frame;

class Main {
    public static var BAG : Bag;

    static function main() {
        BAG = new Bag();

        enable_auto_show();

        var svc : SavedVariable;
        var em = new EventManager();

        em.register("ADDON_LOADED", (name)->{
            if (name == Addon.name) {
                svc = wow.SavedVariable.Load("CatBagCharacterSavedVariables");
                if (svc.save) {
                    svc.bag = svc.save;
                    svc.save = null;
                }

                if (svc.bag) { BAG.load(svc.bag); }
                else { add_defaults(BAG); }
     
            }
        });

        em.register("PLAYER_LOGOUT", ()->{
            svc.bag = BAG.save();
        });


    }

    static function enable_auto_show() {
        var g:AnyTable = untyped __lua__("_G");

        var container_frames:Array<Frame> = [
            g.ContainerFrame1,
            g.ContainerFrame2,
            g.ContainerFrame3,
            g.ContainerFrame4,
            g.ContainerFrame5
        ];
        
        container_frames[0].HookScript("OnShow", () -> {
            for (cf in container_frames) {
                cf.ClearAllPoints();				
                cf.SetPoint(BOTTOMLEFT, wow.G.UIParent, TOPRIGHT, 100,  100);
            }

            BAG.update();
            BAG.show();
        });
        container_frames[0].HookScript("OnHide", ()->{
            BAG.hide();
        });

        var bank_frames:Array<Frame> = [
            g.BankFrame, 
            g.ContainerFrame5,
            g.ContainerFrame6,
            g.ContainerFrame7,
            g.ContainerFrame8,
            g.ContainerFrame9,
            g.ContainerFrame10,
            g.ContainerFrame11,
        ];

        bank_frames[0].HookScript("OnShow", () -> {
            for (bf in bank_frames) {
                // bf.ClearAllPoints();
                // bf.SetPoint(BOTTOMLEFT, wow.G.UIParent, TOPRIGHT, 100,  100);
            }


        });

        // bank_frames[0].HookScript("OnHide", ()->{
        // 	Radio.send("BANK_HIDE");
        // });
    }

    static function add_defaults(bag:Bag) {

        var filters:Array<{				
            name       : String,
            icon       : String,
            code       : String,
            volatile   : Bool,
            auto_hide  : Bool,
            auto_open  : Bool,
            auto_close : Bool,
        }> = [
            {
                name       : "New Apperances",
                icon       : "Interface/MINIMAP/TRACKING/Transmogrifier",
                code       : "return function(item)\n  return item.tooltip:find(\n      \"You haven't collected this appearance\"\n  );\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : true,
                auto_close : false,
            }, //-- [0]
            {
                name       : "Favorites",
                icon       : "Interface/ICONS/ACHIEVEMENT_GUILDPERK_MRPOPULARITY",
                code       : "local favs = {\n  \"Hearthstone\",\n  \"Flight Master's Whistle\",\n  \"Tome of the .* Mind\",\n  \"Augment Rune\",\n};\n\nlocal ids = {\n  157797\n};\n\nreturn function(item)\n  for _,v in ipairs(ids) do\n    if item.id == v then\n      return true;\n    end\n  end\n\n  for k,v in ipairs(favs) do\n    if item.name:match(v) then\n      return true;\n    end\n  end\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [1]
            {
                name       : "Equipment Sets",
                icon       : "Interface/RAIDFRAME/UI-RAIDFRAME-MAINASSIST",
                code       : "return function(item)\n  return item.tooltip:find(\"Equipment Sets\");\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [2]
            {
                name       : "Upgrades",
                icon       : "Interface/ICONS/UI_Mission_ItemUpgrade",
                code       : "return function(item)\n  return item.tooltip:match(\"upgrade\")\n     and item.is_equippable;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [3]
            {
                name       : "Equipment",
                icon       : "Interface/CURSOR/Attack",
                code       : "return function(item)\n  return item.is_equippable\n     and item.quality_id > 0;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [4]
            {
                name       : "Tailoring",
                icon       : "Interface/ICONS/Trade_Tailoring",
                code       : "return function(item)\n  return item.sub_type == \"Cloth\"\n     and not item.is_equippable;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [5]
            {
                name       : "Cooking",
                icon       : "Interface/ICONS/INV_Misc_Food_15",
                code       : "return function(item)\n  return item.sub_type == \"Cooking\"\n      or item.id == 162515;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [6]
            {
                name       : "Reagents",
                icon       : "Interface/MINIMAP/TRACKING/Reagents",
                code       : "return function(item)\n  return item.is_reagent;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [7]
            {
                name       : "Junk",
                icon       : "Interface/ICONS/INV_Misc_Gear_08",
                code       : "return function(item)\n  return item.quality_id == 0\n     and item.sell_price > 0;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [8]
            {
                name       : "Unfiltered",
                icon       : "Interface/PaperDollInfoFrame/UI-GearManager-ItemIntoBag",
                code       : "return function(item)\n  return not item.empty;\nend",
                volatile   : false,
                auto_hide  : true,
                auto_open  : false,
                auto_close : false,
            }, //-- [9]
        ];

        for (fdata in filters) {
            var f = new Filter(fdata.name, fdata.icon, fdata.code);
            f.data.volatile   = fdata.volatile;  
            f.data.auto_hide  = fdata.auto_hide; 
            f.data.auto_open  = fdata.auto_open; 
            f.data.auto_close = fdata.auto_close;
            bag.add_filter(f);
        }
    } 
}