import haxe.extern.EitherType;
import wow.widget.FontString;
import haxe.Unserializer;
import wow.widget.LayeredRegion.DrawLayer;
import wow.widget.Region;
import wow.widget.Texture;
import lua.Table.AnyTable;
import haxe.Constraints.Function;
import wow.widget.Frame;
import wow.widget.Region.Point;
import wow.widget.Button;
import wow.widget.Texture.TexturePath;
import utils.EventManager;

class SlotData {
    public var bag_id  : Int;
    public var slot_id : Int;

    public var icon    : TexturePath;
    public var link    : String;

    public var name        : String;
    public var type        : String;
    public var sub_type    : String;
    public var tooltip     : String;
    public var equip_loc   : String;

    public var level          : Int;
    public var required_level : Int;
    public var count          : Int;
    public var stack_size     : Int;
    public var sell_price     : Int;

    public var id             : Int;
    public var type_id        : Int;
    public var sub_type_id    : Int;
    public var bind_id        : Int;
    public var quality_id     : Int;
    public var expac_id       : Int;
    public var set_id         : Int;
    public var equip_ids      : Array<Int>;

    public var is_locked     : Bool;
    public var is_readable   : Bool;
    public var is_lootable   : Bool;
    public var is_reagent    : Bool;
    public var is_equippable : Bool;
    public var is_usable     : Bool;
    public var is_consumable : Bool;

    public var can_use : Bool;
    public var empty   : Bool;

    public function new(bag:Int, slot:Int) {
        bag_id = bag;
        slot_id = slot;
        clear();
    }

    public function clear() {
        icon           = "Interface\\PaperDoll\\UI-Backpack-EmptySlot";
        link           = "";

        name           = "";
        type           = "";
        sub_type       = "";
        equip_loc      = "";
        tooltip        = "";

        level          = -1;
        required_level = -1;
        count          = -1;
        stack_size     = -1;
        sell_price     = -1;

        id             = -1;
        type_id        = -1;
        sub_type_id    = -1;
        bind_id        = -1;
        quality_id     = -1;
        expac_id       = -1;
        set_id         = -1;
        equip_ids      = [];
        

        is_locked      = false;
        is_readable    = false;
        is_lootable    = false;
        is_reagent     = false;
        is_equippable  = false;
        is_usable      = false;
        is_consumable  = false;
        
        can_use        = false;
        empty          = true;
    }
}

class Slot {
    var bag : Bag;

    var parent       : Frame;
    var button       : Button;
    var background   : Texture;
    var border       : Texture;
    var number       : FontString;
    var junk_icon    : Texture;
    var upgrade_icon : Texture;
    var _button      : AnyTable;

    public var data(default, null) : SlotData;

    public function new(bag : Bag, bag_id : Int, slot_id : Int) {
        this.bag = bag;
        data = new SlotData(bag_id, slot_id);
        create_frame();
    }

    public function update() : Void {
        var id = wow.Bag.GetContainerItemID(data.bag_id, data.slot_id);
        if (id == null) return data.clear();
        data.id    = id;
        data.empty = false;

        var bag_info = wow.Bag.GetContainerItemInfo(data.bag_id, data.slot_id);

        data.icon        = bag_info.texture;
        data.link        = bag_info.link;
        
        data.count       = bag_info.count;
        data.quality_id  = bag_info.quality_id;
        
        data.is_locked   = !!bag_info.locked;
        data.is_readable = !!bag_info.readable;
        data.is_lootable = !!bag_info.lootable;

        var level_info = wow.Item.GetDetailedItemLevelInfo(data.link);
        data.level = level_info.effective;

        var item_info = wow.Item.GetItemInfo(id);

        data.name           = item_info.name;
        data.type           = item_info.type;
        data.sub_type       = item_info.sub_type;
        data.equip_loc      = item_info.equip_loc;

        data.required_level = item_info.required_level;
        data.stack_size     = item_info.stack_size;
        data.sell_price     = item_info.sell_price;

        data.type_id        = item_info.type_id;
        data.sub_type_id    = item_info.sub_type_id;
        data.bind_id        = item_info.bind_type_id;
        data.expac_id       = item_info.expac_id;
        data.set_id         = item_info.set_id != null ? item_info.set_id : -1;
        
        data.is_reagent    = !!item_info.is_crafting_reagent;
        data.is_usable     = !!wow.Item.IsUsableItem(data.link).usable;
        data.is_equippable = !!wow.Item.IsEquippableItem(data.link);
        data.is_consumable = !!wow.Item.IsConsumableItem(data.link);

        var scan = utils.TooltipScanner.scan_bag_slot_tooltip(data.bag_id, data.slot_id);
        data.tooltip = scan.tooltip;
        data.can_use = !scan.has_red;

        data.equip_ids = equip_loc_to_ids(data.equip_loc);

        /// internal data used by ContainerItemFrame_OnClick
        _button.hasItem = !data.empty;
    }



    public function is_empty() {
        return data.empty;
    }

    public function hide() {
        parent.Hide();
    }

    public function use() {
        button.Click(ClickType.RightButtonUp);
    }


    /**(button, quality, itemIDOrLink, suppressOverlays)**/
    static final SetItemButtonQuality:Function = untyped __lua__("SetItemButtonQuality");

    public function redraw() {
        button.SetNormalTexture(data.icon);
        background.SetTexture(data.icon);

        SetItemButtonQuality(_button, data.quality_id, data.id, false);
        border.Show();
        if (data.quality_id <= 0) {
            border.SetVertexColor(0.4,0.4,0.4);
        } else if (data.quality_id == 1) {
            border.SetVertexColor(0.8,0.8,0.8);
        }

        junk_icon.SetShown(data.quality_id == 0 && data.sell_price > 0);

        number.SetText(data.count > 1 ? cast data.count : null);

        if (!data.can_use && !is_empty()) {
            background.SetVertexColor(0.8,0.2,0.2,1);
        } else {
            background.SetVertexColor(1,1,1,1);
        }
    }

    public function anchor(point :Point, parent:Frame, p_point :Point, x:Float, y:Float) {
        this.parent.SetParent(parent);
        this.parent.Show();
        button.ClearAllPoints();
        button.SetPoint(point, parent, p_point, x, y);
    }


    function create_frame() {
        parent = wow.G.CreateFrame(FRAME);
        parent.SetID(data.bag_id);
        
        _button = wow.G.CreateFrame(BUTTON, "CatBagSlot[" + data.bag_id + "," + data.slot_id + "]", parent, "ContainerFrameItemButtonTemplate");
        button = cast _button;
        button.GetNormalTexture().SetAllPoints(button);
        button.GetNormalTexture().SetAlpha(0);
        button.SetID(data.slot_id);
        button.Show();

        number = cast (_button,AnyTable).Count;
        number.SetPoint(BOTTOMRIGHT, button, BOTTOMRIGHT, -3, 3);
        number.Show();

        // button.SetText("0");
        // var text = button.GetFontString();
        // text.ClearAllPoints();
        // text.SetFont(Style.FONT, Style.FONT_SIZE, "OUTLINE");

        _button.NewItemTexture.Hide();
        _button.BattlepayItemTexture.Hide();       

        border = _button.IconBorder;
        background = button.CreateTexture(null, BACKGROUND);
        background.SetAllPoints(button);

        button.HookScript("OnClick",(self, btn)->{
            if (untyped __lua__("IsAltKeyDown()")) {
                widget.SlotInfo.INSTANCE.view(data);
            }
        });

        junk_icon    = _button.JunkIcon;
        upgrade_icon = _button.UpgradeIcon;

        var em = new EventManager();
        em.register("SCRAPPING_MACHINE_SHOW", ()->{
            wow.G.print("HI");
        });

        if (data.bag_id == -1) {
            em.register("PLAYERBANKSLOTS_CHANGED", (slot)->{
                if (slot == data.slot_id) {
                    update();
                    redraw();
                }
            });
        } else if (data.bag_id == -3) {
            em.register("PLAYERREAGENTBANKSLOTS_CHANGED", (slot)->{
                if (slot == data.slot_id) {
                    update();
                    redraw();
                }
            });
        } else {
            em.register("BAG_UPDATE", (bag)->{
                if (bag == data.bag_id) {
                    update();
                    redraw();                    
                }
            });
        }


    }

    public function click() {
        button.Click();
    }


    static function equip_loc_to_ids(equip_loc : String) : Array<Int> {
        if (equip_loc == "INVTYPE_AMMO")           return [0];
        if (equip_loc == "INVTYPE_HEAD")           return [1];
        if (equip_loc == "INVTYPE_NECK")           return [2];
        if (equip_loc == "INVTYPE_SHOULDER")       return [3];
        if (equip_loc == "INVTYPE_BODY")           return [4];
        if (equip_loc == "INVTYPE_CHEST")          return [5];
        if (equip_loc == "INVTYPE_ROBE")           return [5];
        if (equip_loc == "INVTYPE_WAIST")          return [6];
        if (equip_loc == "INVTYPE_LEGS")           return [7];
        if (equip_loc == "INVTYPE_FEET")           return [8];
        if (equip_loc == "INVTYPE_WRIST")          return [9];
        if (equip_loc == "INVTYPE_HAND")           return [10];
        if (equip_loc == "INVTYPE_FINGER")         return [11,12];
        if (equip_loc == "INVTYPE_TRINKET")        return [13,14];
        if (equip_loc == "INVTYPE_CLOAK")          return [15];
        if (equip_loc == "INVTYPE_WEAPON")         return [16,17];
        if (equip_loc == "INVTYPE_SHIELD")         return [17];
        if (equip_loc == "INVTYPE_2HWEAPON")       return [16];
        if (equip_loc == "INVTYPE_WEAPONMAINHAND") return [16];
        if (equip_loc == "INVTYPE_WEAPONOFFHAND")  return [17];
        if (equip_loc == "INVTYPE_HOLDABLE")       return [17];
        if (equip_loc == "INVTYPE_RANGED")         return [18];
        if (equip_loc == "INVTYPE_THROWN")         return [18];
        if (equip_loc == "INVTYPE_RANGEDRIGHT")    return [18];
        if (equip_loc == "INVTYPE_RELIC")          return [18];
        if (equip_loc == "INVTYPE_TABARD")         return [19];
        if (equip_loc == "INVTYPE_BAG")            return [20,21,22,23];
        if (equip_loc == "INVTYPE_QUIVER")         return [20,21,22,23];
        return [];
    }
}