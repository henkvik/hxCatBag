package wow;
import wow.widget.Texture.TexturePath;
import haxe.extern.EitherType;
import haxe.EnumFlags;

@:native("_G")
extern class Item {
    
    // static function GetContainerItemLink(bagID, slot) // - Returns the itemLink of the item located in bag#, slot#.
    static function EquipItemByName(item : EitherType<Int, String>, ?invSlot : Int) : Void; // - Equips an item, optionally into a specified slot.
    static function GetAuctionItemLink(type : String, index : Int) : String; // - Returns an itemLink for the specified auction item.
    static function GetDetailedItemLevelInfo(item : EitherType<Int, String>) : ItemLevelInfo; // - Returns an item's item level
    static function GetItemCooldown(item_id : Int) : ItemCooldown; // - Returns startTime, duration, enable.
    static function GetItemCount(item : EitherType<Int, String>, ?include_bank : Bool, ?include_charges : Bool) : Int; // - Returns number of such items in inventory[, or charges instead if it has charges]
    static function GetItemFamily(item : EitherType<Int, String>) : Int; // - Returns the bag type that an item can go into, or for bags the type of items that it can contain. (added in Patch 2.4)
    static function GetItemIcon(item_id : Int) : Int; // - Returns the icon for the item. Works for any valid item even if it's not in the cache. (added in Patch 2.4)
    static function GetItemInfo(item : EitherType<Int, String>) : ItemInfo; // - Returns information about an item.
    static function GetItemInfoInstant(item : EitherType<Int, String>) : ItemInfoInstant; // - Returns basic information about an item.
    static function GetItemQualityColor(quality : Int) : ItemColor; // - Returns the RGB color codes for a quality.
    static function GetItemSpell(item : EitherType<Int, String>) : ItemSpell; // - Returns name, rank.
    static function GetItemStats(item_link : String) : ItemStats; // - Returns a table of stats for an item.
    static function GetMerchantItemLink(index : Int) : String; // - Returns an itemLink for the given purchasable item
    static function GetQuestItemLink(type : String, index : Int) : String; // - Returns an itemLink for a quest reward item.
    static function GetQuestLogItemLink(type : String, index : Int) : String; // - Returns an itemLink for a quest reward item.
    static function GetTradePlayerItemLink(index : Int) : String; // - Returns an itemLink for the given item in your side of the trade window (if open)
    static function GetTradeSkillItemLink(index : Int) : String; // - Returns the itemLink for a trade skill item.
    static function GetTradeSkillReagentItemLink(index : Int, reagent_indexd : Int) : String; // - Returns the itemLink for one of the reagents needed to craft the given item
    static function GetTradeTargetItemLink(index : Int) : String; // - Returns an itemLink for the given item in the other player's side of the trade window (if open)
    static function IsUsableItem(item : EitherType<Int, String>) : ItemUsableInfo; // - Returns usable, noMana.
    static function IsConsumableItem(item : EitherType<Int, String>) : Bool; //-
    static function IsCurrentItem(item : EitherType<Int, String>) : Bool; //-
    static function IsEquippedItem(item : EitherType<Int, String>) : Bool; //-
    static function IsEquippableItem(item : EitherType<Int, String>) : Bool; // - Returns whether an item can be equipped.
    static function IsEquippedItemType(type : String) : Bool; // - Where "type" is any valid inventory type, item class, or item subclass.
    static function IsItemInRange(item : EitherType<Int, String>, target : String) : Bool; // - Nil for invalid target, false for out of range, true for in range.
    static function ItemHasRange(item : EitherType<Int, String>) : Bool; // -
    static function UseItemByName(item_name : String, target : String) : Void; //PROTECTED  - Use an item on the unit specified. 
    static function SetItemRef(link : String, text : String, button : String) : Void; //UI  - Handles item link tooltips in chat.
}

@:multiReturn
extern class ItemCooldown {
    var start : Float;
    var duration : Float;
    var active : Bool;
}

@:multiReturn
extern class ItemInfo {
    var name : String; // 	The name of the item.
    var link : String; // 	The item link of the item.
    var quality_id : Int; // 	The quality of the item. The value is 0 to 7, which represents Poor to Heirloom. This appears to include gains from upgrades/bonuses.
    var level : Int; // 	The item level of this item, not including item levels gained from upgrades. There is currently no API to get the item level including upgrades/bonuses.
    var required_level : Int; // 	The minimum level required to use the item, 0 meaning no level requirement.
    var type : String; // 	The type of the item: Armor, Weapon, Quest, Key, etc.
    var sub_type : String; // 	The sub-type of the item: Enchanting, Cloth, Sword, etc. See itemType.
    var stack_size : Int; // 	How many of the item per stack: 20 for Runecloth, 1 for weapon, 100 for Alterac Ram Hide, etc.
    var equip_loc : String; // 	The type of inventory equipment location in which the item may be equipped, or "" if it can't be equippable. The string returned is also the name of a global string variable e.g. if "INVTYPE_WEAPONMAINHAND" is returned, _G["INVTYPE_WEAPONMAINHAND"] will be the localized, displayable name of the location.
    var icon_id : TexturePath; // 	The FileDataID for the icon texture for the item.
    var sell_price : Int; // 	The price, in copper, a vendor is willing to pay for this item, 0 for items that cannot be sold.
    var type_id : Int; // 	This is the numerical value that determines the string to display for 'itemType'.
    var sub_type_id : Int; // 	This is the numerical value that determines the string to display for 'itemSubType'
    var bind_type_id : Int; // 	Item binding type: 0 - none; 1 - on pickup; 2 - on equip; 3 - on use; 4 - quest.
    var expac_id : Int; //
    var set_id : Int; //
    var is_crafting_reagent : Bool; //
}

@:multiReturn
extern class ItemInfoInstant {
    var itemID : Int; // 	ID of the item.
    var itemType : String; // 	The type of the item: Armor, Weapon, Quest, Key, etc.
    var itemSubType : String; // 	The sub-type of the item: Enchanting, Cloth, Sword, etc. See itemType.
    var itemEquipLoc : String; // 	The type of inventory equipment location in which the item may be equipped, or "" if it can't be equippable. The string returned is also the name of a global string variable e.g. if "INVTYPE_WEAPONMAINHAND" is returned, _G["INVTYPE_WEAPONMAINHAND"] will be the localized, displayable name of the location.
    var iconFileDataID : Int; // 	The FileDataID for the icon texture for the item.
    var itemClassID : Int; // 	This is the numerical value that determines the string to display for 'itemType'.
    var itemSubClassID : Int; // 	This is the numerical value that determines the string to display for 'itemSubType'
}

@:multiReturn 
extern class ItemColor {
    var r : Float;
    var g : Float;
    var b : Float;
    var hex : String;
}

@:multiReturn
extern class ItemSpell {
    var name : String;
    var rank : String;
    var id : Int;
}

@:multiReturn 
extern class ItemUsableInfo {
    var usable : Bool;
    var no_mana : Bool;
}

extern class ItemStats {}

@:enum
abstract ItemFamily(Int) {
    var Normal = 0;
    var Quiver = 1;
    var AmmoPouch = 2;
    var SoulBag = 4;
    var LeatherworkingBag = 8;
    var InscriptionBag = 16;
    var HerbBag = 32;
    var EnchantingBag = 64;
    var EngineeringBag = 128;
    var Keyring = 256;
    var GemBag = 512;
    var MiningBag = 1024;
    var Unknown = 2048;
    var VanityPets = 4096;
}

@:multiReturn
extern class ItemLevelInfo {
    var effective : Int;
    var preview : Bool;
    var base : Int; 
}

// @:enum
// abstract ItemBindType(Int) {
//     var None = 0;
//     var Pickup = 1;
//     var Equip = 2;
//     var Use = 3;
//     var Quest = 4;
// }

// @:enum
// abstract ItemQuality(Int) {
//     var Poor = 0; // 9d9d9d
//     var Common = 1; // ffffff
//     var Uncommon = 2; // 1eff00
//     var Rare = 3; // 0070dd
//     var Epic = 4; // a335ee
//     var Legendary = 5; // ff8000
//     var Artifact = 6; // e6cc80
//     var Heirloom = 7; // e6cc80
// }