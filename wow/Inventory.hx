package wow;

import wow.widget.Texture.TexturePath;

@:native("_G")
extern class Inventory {
    
    static function AutoEquipCursorItem():Void;                                                     //() - Causes the equipment on the cursor to be equipped.
    static function BankButtonIDToInvSlotID(index:Int, is_bag : Bool):Int;                          //(buttonID, isBag) - Returns the ID number of a bank button or bag in terms of inventory slot ID.
    static function CancelPendingEquip(inv_id:Int):Void;                                            //(index) - This function is used to cancel a pending equip.
    static function ConfirmBindOnUse():Void;                                                        //()
    static function ContainerIDToInventoryID(bag_id:Int):Int;                                       //(bagID)
    static function CursorCanGoInSlot():Bool;                                                       //(invSlot) - Return true if the item currently held by the cursor can go into the given inventory (equipment) slot
    static function EquipCursorItem(inv_id:Int):Void;                                                   //(invSlot)
    static function EquipPendingItem(inv_id:Int):Void;                                              //(invSlot) - Equip//(Internal — do not use.)
    static function GetAverageItemLevel():AverageItemLevelInfo;                                     //() - Return the character's current average iLevel and current average iLevel equipped.
    static function GetInventoryAlertStatus(inv_id:Int):Int;                                        //(index) - Returns one of several codes describing the "status" of an equipped item.
    static function GetInventoryItemBroken(unit:String, inv_id:Int):Bool;                           //("unit", invSlot) - Determine if an inventory item is broken (no durability).
    static function GetInventoryItemCooldown(unit:String, inv_id:Int):InventoryItemCooldownInfo;    //("unit", invSlot) - Get cooldown information for an inventory item.
    static function GetInventoryItemCount(unit:String, inv_id:Int):Int;                             //("unit", invSlot) - Determine the quantity of an item in an inventory slot.
    static function GetInventoryItemDurability(inv_id:Int):InventoryItemDurabilityInfo;             //(invSlot) - Returns the maximum and remaining durability points for an inventory item.
    static function GetInventoryItemID(unit:String, inv_id:Int):Int;                                //("unit", invSlot) - Returns the item id of the item in the specified inventory slot.
    static function GetInventoryItemLink(unit:String, inv_id:Int):String;                           //("unit", invSlot) - Returns an itemLink for an inventory (equipped) item.
    static function GetInventoryItemQuality(unit:String, inv_id:Int):Int;                           //("unit", invSlot) - Return the quality of an inventory item.
    static function GetInventoryItemTexture(unit:String, inv_id:Int):TexturePath;                   //("unit", invSlot) - Return the texture for an inventory item.
    static function GetInventorySlotInfo(slot_name:String):InventorySlotInfo;                       //(invSlotName) - Get the info for a named inventory slot (slot ID and texture)
    static function GetWeaponEnchantInfo():WeaponEnchantInfo;                                       //() - Return information about main and offhand weapon enchantments.
    static function HasWandEquipped():Bool;                                                         //() - Returns 1 if a wand is equipped, false otherwise.
    static function IsInventoryItemLocked(inv_id:Int):Bool;                                         //(id) - Returns whether an inventory item is locked, usually as it awaits pending action.
    static function OffhandHasWeapon():Bool;                                                        //() - Determine if your offhand carries a weapon.
    static function PickupBagFromSlot(inv_id:Int):Void;                                             //(slot) - Picks up the bag from the specified slot, placing it in the cursor. If an item is already picked up, this places the item into the specified slot, swapping the items if needed.
    static function PickupInventoryItem(inv_id:Int):Void;                                           //(invSlot) - "Picks up" an item from the player's worn inventory.
    static function UpdateInventoryAlertStatus():Void;                                              //()
    static function UseInventoryItem(inv_id:Int):Void;                                              //(invSlot) - PROTECTED Use an item in a specific inventory slot. 
}

@:multiReturn
extern class AverageItemLevelInfo {
    var avg_item_level:Float;
    var avg_item_level_equipped:Float;
}

@:multiReturn
extern class InventoryItemCooldownInfo {
    var start    : Float; //Numeric - The start time of the cooldown period, or 0 if there is no cooldown (or no item in the slot)
    var duration : Float; //Numeric - The duration of the cooldown period (NOT the remaining time). 0 if the item has no use/cooldown or the slot is empty.
    var enable   : Int;  //Numeric - Returns 1 or 0. 1 if the inventory item is capable of having a cooldown, 0 if not.
}

@:multiReturn
extern class InventoryItemDurabilityInfo {
    var current : Int;
    var maximum : Int;
}

@:multiReturn
extern class InventorySlotInfo {
    var inv_id  : Int;         //Number - The slot ID to use to refer to that slot in the other GetInventory functions.
    var texture : TexturePath; //String - The texture to use for the empty slot on the paper doll display. 
}

@:multiReturn
extern class WeaponEnchantInfo {    
    var has_main_hand_enchant : Bool;  //Flag - true if the weapon in the main hand slot has a temporary enchant, false otherwise
    var main_hand_expiration  : Float; //Number - time remaining for the main hand enchant, as thousandths of seconds
    var main_hand_charges     : Float; //Number - the number of charges remaining on the main hand enchant
    var main_hand_enchant_id  : Float; //Number - ID of the main hand enchant (new in 6.0)
    var has_off_hand_enchant  : Bool;  //Flag - true if the weapon in the secondary (off) hand slot has a temporary enchant, false otherwise
    var off_hand_expiration   : Float; //Number - time remaining for the off hand enchant, as thousandths of seconds
    var off_hand_charges      : Float; //Number - the number of charges remaining on the off hand enchant
    var off_hand_enchant_id   : Float; //Number - ID of the off hand enchant (new in 6.0) 
}