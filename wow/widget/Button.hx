package wow.widget;

import wow.widget.Texture.TexturePath;
import wow.widget.Texture.AlphaMode;
import haxe.extern.EitherType;
import wow.G.FloatXY;

extern class Button extends Frame {
    function Click(?button:EitherType<String,ClickType>) : Void;        // - Execute the click action of the button.
    function Disable() : Void;      // - Disable the Button so that it cannot be clicked.
    function Enable() : Void;       // - Enable to the Button so that it may be clicked.
    function GetButtonState() : ButtonState;        // - Return the current state ("PUSHED","NORMAL") of the Button.
    function GetDisabledFontObject() : Font;        // - Return the font object for the Button when disabled - New in 1.10.
    function GetDisabledTexture() : Texture;        // - Get the texture for this button when disabled - New in 1.11.
    function GetFontString() : FontString;          // - Get this button's label FontString - New in 1.11.
    function GetHighlightFontObject() : Font;       // - Return the font object for the Button when highlighted - New in 1.10.
    function GetHighlightTexture() : Texture;       // - Get the texture for this button when highlighted.
    function GetMotionScriptsWhileDisabled() : Bool;       // - Get whether the button is allowed to run its OnEnter and OnLeave scripts even while disabled - New in 3.3.
    function GetNormalTexture() : Texture;     // - Get the normal texture for this button - New in 1.11.
    function GetNormalFontObject() : Font;      // - Get the Font Object of the button.
    function GetPushedTextOffset() : FloatXY;           // - Get the text offset when this button is pushed (x, y)      // - New in 1.11.
    function GetPushedTexture() : Texture;      // - Get the texture for this button when pushed - New in 1.11.
    function GetText() : String;                // - Get the text label for the Button.
    function GetTextHeight() : Float;           // - Get the height of the Button's text.
    function GetTextWidth() : Float;        // - Get the width of the Button's text.
    function IsEnabled() : Bool;            // - Determine whether the Button is enabled.
    function LockHighlight() : Void;        // - Set the Button to always be drawn highlighted.
    function RegisterForClicks(click_type : haxe.extern.Rest<ClickType>) : Void;        // - Specify which mouse button up/down actions cause this button to receive an OnClick notification.
    function SetButtonState(state : ButtonState, ?lock : Bool) : Void;        // - Set the state of the Button ("PUSHED", "NORMAL") and whether it is locked.
    function SetDisabledFontObject(?font : Font) : Void;      // - Set the font object for settings when disabled - New in 1.10.
    function SetDisabledTexture(texture : EitherType<Texture, TexturePath>) : Void;       // - Set the disabled texture for the Button - Updated in 1.10.
    function SetFont(font : String, size : Float, ?flags : String) : Void;        // - Set the font to use for display.
    function SetFontString(font_string : FontString) : Void;      // - Set the button's label FontString - New in 1.11.
    function SetHighlightFontObject(?font : Font) : Void;    // - Set the font object for settings when highlighted - New in 1.10.
    function SetHighlightTexture(texture : EitherType<Texture, TexturePath>, alpha_mode : AlphaMode = ADD) : Void;      // - Set the highlight texture for the Button.
    function SetMotionScriptsWhileDisabled(?bool : Bool) : Void;      // - Set whether button should fire its OnEnter and OnLeave scripts even while disabled - New in 3.3.
    function SetNormalTexture(texture : EitherType<Texture, TexturePath>) : Void;    // - Set the normal texture for the Button - Updated in 1.10.
    function SetNormalFontObject(font_object : Font) : Void;        // - Set the Font Object of the button.
    function SetPushedTextOffset(x : Font, y : Font) : Void;      // - Set the text offset for this button when pushed - New in 1.11.
    function SetPushedTexture(texture : EitherType<Texture, TexturePath>) : Void;     // - Set the pushed texture for the Button - Updated in 1.10.
    function SetText(text : String) : Void;        // - Set the text label for the Button.
    function UnlockHighlight() : Void;      // - Set the Button to not always be drawn highlighted. 
    // function SetFormattedText("formatstring"[, ...])        // - Set the formatted text label for the Button. - New in 2.3.
}

@:enum
abstract ButtonState(String) {
    var PUSHED;
    var NORMAL;
}

@:enum
abstract ClickType(String) {
    var LeftButtonUp;
    var RightButtonUp;
    var MiddleButtonUp;
    var Button4Up;
    var Button5Up;
    var LeftButtonDown;
    var RightButtonDown;
    var MiddleButtonDown;
    var Button4Down;
    var Button5Down;
    var AnyUp;
    var AnyDown; 
}