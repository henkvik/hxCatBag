package wow.widget;

extern class GameTooltip extends Frame {

    function SetOwner(owner:Frame, anchor:AnchorType, ?x:Float, ?y:Float) :Void; // -
    function AddFontStrings(left:FontString, right:FontString) :Void; // - Dynamically expands the size of a tooltip - New in 1.11.
    function SetBagItem(bag:Int, slot:Int) :Void; // -
    function SetInventoryItem(unit :String, slot : Int, ?nameOnly:Bool) :Void;
    function ClearLines() :Void; // - Clear all lines of tooltip (both left and right ones)


/**
    function AddDoubleLine(textL, textR, rL, gL, bL, rR, gR, bR) :;
    function AddLine("tooltipText" [, textColor.r, textColor.g, textColor.b [, wrapText]]) :; // - Appends the new line to the tooltip.
    function AddSpellByID(spellID) :; // -
    function AddTexture("texture") :; // - Add a texture to the last line added.
    function AdvanceSecondaryCompareItem() :; // -
    function AppendText("text") :; // - Append text to the end of the first line of the tooltip.
    function FadeOut() :; // -
    function GetAnchorType() :; // - Returns the current anchoring type.
    function GetItem() :; // - Returns name, link.
    function GetMinimumWidth() :; // -
    function GetOwner() :; // - Returns owner frame, anchor.
    function GetPadding() :; // -
    function GetSpell() :; // - Returns name, rank, id.
    function GetUnit() :; // - Returns unit name, unit id.
    function IsEquippedItem() :; // -
    function IsUnit("unit") :; // - Returns bool.
    function NumLines() :; // - Get the number of lines in the tooltip.
    function ResetSecondaryCompareItem() :; // -
    function SetAchievementByID(id) :; // -
    function SetAction(slot) :; // - Shows the tooltip for the specified action button.
    function SetAnchorType(anchorType [,Xoffset] [,Yoffset]) :; // -
    function SetAuctionCompareItem("type", index[, offset])
    function SetAuctionItem("type", index) :; // - Shows the tooltip for the specified auction item.
    function SetAuctionSellItem() :; // -
    function SetBackpackToken(id) :; // -
    function SetBuybackItem(slot) :; // -
    function SetCompareItem(shoppingTooltipTwo, primaryMouseover) :; // -
    function SetCurrencyByID(id) :; // -
    function SetCurrencyToken(tokenId) :; // - Shows the tooltip for the specified token
    function SetCurrencyTokenByID(currencyID) :; // -
    function SetEquipmentSet(name) :; // - Shows details for the equipment manager set identified by name.
    function SetExistingSocketGem(index, [toDestroy]) :; // -
    function SetFrameStack(showhidden) :; // - Shows the mouseover frame stack, used for debugging.
    function SetGlyph(id) :; // -
    function SetGlyphByID(glyphID) :; // -
    function SetGuildBankItem(tab, id) :; // - Shows the tooltip for the specified guild bank item
    function SetHeirloomByItemID(itemID) :; // -
    function SetHyperlink("itemString" or "itemLink") :; // - Changes the item which is displayed in the tooltip according to the passed argument.
    function SetInboxItem(index) :; // - Shows the tooltip for the specified mail inbox item.
    function SetInstanceLockEncountersComplete(index) :; // -
    function SetInventoryItemByID(itemID) :; // -
    function SetItemByID(itemID) :; // - Shows the tooltip for a specified Item ID. (added in 4.2.0.14002 along with the Encounter Journal)
    function SetLFGCompletionReward(lootIndex) :; // -
    function SetLFGDungeonReward(dungeonID, lootIndex) :; // -
    function SetLFGDungeonShortageReward(dungeonID, shortageSeverity, lootIndex)) :; // -
    function SetLootCurrency(lootSlot) :; // -
    function SetLootItem(lootSlot) :; // -
    function SetLootRollItem(id) :; // - Shows the tooltip for the specified loot roll item.
    function SetMerchantCostItem(slot) :; // -
    function SetMerchantItem(merchantSlot) :; // -
    function SetMinimumWidth(width) :; // - (Formerly SetMoneyWidth)
    function SetMissingLootItem(index) :; // -
    function SetMountBySpellID() :; // -
    function SetPadding() :; // -
    function SetPetAction(slot) :; // - Shows the tooltip for the specified pet action.
    function SetPossession(slot) :; // -
    function SetPvPReward(ID, isCurrency) :; // -
    function SetQuestCurrency("type", index) :; // -
    function SetQuestItem("type", index) :; // -
    function SetQuestLogCurrency("type", index) :; // -
    function SetQuestLogItem("type", index)
    function SetQuestLogRewardSpell - Shows the tooltip for the spell reward of the currently selected quest.
    function SetQuestLogSpecialItem(index) :; // -
    function SetQuestRewardSpell
    function SetSendMailItem() :; // -
    function SetShapeshift(slot) :; // - Shows the tooltip for the specified shapeshift form.
    function SetSocketedItem() :; // -
    function SetSocketGem(index) :; // -
    function SetSpellBookItem(spellId, bookType) :; // - Shows the tooltip for the specified spell in the spellbook.
    function SetSpellByID(spellId) :; // - Shows the tooltip for the specified spell by global spell ID.
    function SetTalent(talentIndex [, isInspect, talentGroup, inspectedUnit, classId]) :; // - Shows the tooltip for the specified talent.
    function SetText("text", r, g, b[, alphaValue[, textWrap]]) :; // - Set the text of the tooltip.
    function SetTotem(slot) :; // -
    function SetToyByItemID(itemID) :; // -
    function SetTradePlayerItem(tradeSlot) :; // -
    function SetTradeSkillItem(index [,reagent]) :; // -
    function SetTradeTargetItem(tradeSlot) :; // -
    function SetTrainerService(index) :; // -
    function SetTransmogrifyItem(slotId) :; // - Shows the tooltip when there is a pending (de)transmogrification
    function SetUnit(unit : String) :Void;// - Shows the tooltip for a particular unit
    function SetUnitAura("unitId", auraIndex[, filter]) :; // - Shows the tooltip for a unit's aura. (Exclusive to 3.x.x / WotLK)
    function SetUnitBuff("unitId", buffIndex[, raidFilter]) :; // - Shows the tooltip for a unit's buff.
    function SetUnitConsolidatedBuff("unit", buffIndex) :; // -
    function SetUnitDebuff("unitId", buffIndex[, raidFilter]) :; // - Shows the tooltip for a unit's debuff.
    function SetUpgradeItem() :; // -
    function SetVoidDepositItem(slotIndex) :; // - Shows the tooltip for the specified Void Transfer deposit slot (added in 4.3.0)
    function SetVoidItem(slotIndex) :; // - Shows the tooltip for the specified Void Storage slot (added in 4.3.0)
    function SetVoidWithdrawalItem(slotIndex) :; // - Shows the tooltip for the specified Void Transfer withdrawal slot (added in 4.3.0) 
        
    // REMOVED function SetTracking
    // REMOVED function SetReforgeItem - Shows the tooltip for the reforge item
    // REMOVED function SetMerchantCompareItem("slot"[, offset])
    // REMOVED function SetHyperlinkCompareItem("itemLink", index) :; // - Sets a comparison tooltip to show the index th comparison item to the item specified as link. Will return a true value if there is an index th comparison item (index is 1 through 3)
**/
}


@:enum
abstract AnchorType(String) {    
    var ANCHOR_TOP; 
    var ANCHOR_RIGHT; 
    var ANCHOR_BOTTOM; 
    var ANCHOR_LEFT; 
    var ANCHOR_TOPRIGHT; 
    var ANCHOR_BOTTOMRIGHT; 
    var ANCHOR_TOPLEFT; 
    var ANCHOR_BOTTOMLEFT; 
    var ANCHOR_CURSOR; 
    var ANCHOR_PRESERVE; 
    var ANCHOR_NONE; 
}