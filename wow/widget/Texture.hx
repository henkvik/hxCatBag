package wow.widget;
import haxe.extern.EitherType;

extern class Texture extends LayeredRegion {
    // function AddMaskTexture(maskTexture)
    // function RemoveMaskTexture(maskTexture)
    // function SetGradient("orientation", minR, minG, minB, maxR, maxG, maxB)
    // function SetGradientAlpha("orientation", minR, minG, minB, minA, maxR, maxG, maxB, maxA)
    // function SetRotation(angle, [,cx, cy])// - Applies a counter-clockwise rotation to the texture.
    function GetBlendMode() : AlphaMode; // - Return the blend mode set by SetBlendMode()
    function GetTexCoord() : TexCoords;// - Gets the 8 texture coordinates that map to the Texture's corners// - New in 1.11.
    function GetTexture() : String; // - Gets this texture's current texture path.
    function GetVertexColor() : VertexColor;// - Gets the vertex color for the Texture.
    function IsDesaturated() : Bool; // - Gets the desaturation state of this Texture.// - New in 1.11
    function SetBlendMode(mode : AlphaMode) : Void;// - Set the alphaMode of the texture.
    function SetDesaturated(flag : Bool) : Void;// - Set whether this texture should be displayed with no saturation (Note: This has a return value)
    function SetHorizTile(horizTile : Bool) : Void; // - Sets whether the widget should adjust horizontal texture coordinates based on texture and widget dimensions.
    function SetTexCoord(minX : Float, maxX : Float, minY : Float, maxY : Float) : Void;// - Modifies the region of a texture drawn by the Texture widget.
    @:native("SetTexCoord") function SetTexCoord1(ULx : Float, ULy : Float, LLx : Float, LLy : Float, URx : Float, URy : Float, LRx : Float, LRy : Float) : Void; // - Modifies the region of a texture drawn by the Texture widget.
    function SetTexture(file_path : TexturePath, ?horizWrap : WrapMode, ?vertWrap : WrapMode, ?filterMode : FilterMode) : Void; // - Changes the texture displayed by the Texture widget.
    function SetColorTexture(r : Float, g : Float, b : Float, ?a : Float) : Void; // - Changes the color of a texture.
    function SetVertTile(horizTile: Bool) : Void;// - Sets whether the widget should adjust vertical texture coordinates based on texture and widget dimensions.
}

extern class MaskTexture extends Texture {}

abstract TexturePath(EitherType<String, Int>) from EitherType<String, Int> to EitherType<String, Int> {}

@:enum 
abstract AlphaMode(String) {
    var DISABLE; // - opaque texture
    var BLEND; // - normal painting on top of the background, obeying alpha channels if present in the image (uses alpha)
    var ALPHAKEY; // - one-bit alpha
    var ADD; // - additive blend
    var MOD; // - modulating blend
}

@:enum 
abstract WrapMode(String) {
    var CLAMP; // (default) 	The edges of the texture are repeated infinitely.
    var CLAMPTOBLACK; // 	Fills the rest of the texture coordinate space with black.
    var CLAMPTOBLACKADDITIVE; // 	Fills the rest of the texture coordinate space with a transparent black.
    var CLAMPTOWHITE; // 	Fills the rest of the texture coordinate space with white.
    var REPEAT; // (or true) 	The texture is repeated (in its entirety) infinitely.
    var MIRROR; // 	The texture is repeated (in its entirety) infinitely, with adjacent instances mirrored.
}
@:enum 
abstract FilterMode(String) {
    var LINEAR; // (default; bilinear filtering), 
    var TRILINEAR; // (also sampling mipmaps), or 
    var NEAREST; // (nearest-neighbor filtering).
}

@:multiReturn
extern class TexCoords {
    var ULx : Float;
    var ULy : Float;
    var LLx : Float;
    var LLy : Float;
    var URx : Float;
    var URy : Float;
    var LRx : Float;
    var LRy : Float;
}

@:multiReturn
extern class VertexColor {
    var r : Float;
    var g : Float;
    var b : Float;
    var a : Float;
}



