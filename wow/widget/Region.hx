package wow.widget;
import wow.G.FloatXY;
import haxe.extern.EitherType;

extern class Region extends UIObject {
    
    // function CreateAnimationGroup(["name"[,"inheritsFrom"]])// - Create and return a new AnimationGroup as a child of this Region.// - New in 3.1.0
    // function GetAnimationGroups()// - Returns all AnimationGroups that are children of this Region.// - New in 3.1.0
    function StopAnimating() : Void; // - Stops any active animations on the Region and its children.// - New in 3.1.0 
    
    /**Get details for an anchor point for this frame (point, relativeTo, relativePoint, xofs, yofs)// - New in 1.10.**/
    function GetPoint(index : Int) : PointData;
    /**Get the number of anchor points for this frame// - New in 1.10.**/
    function GetNumPoints() : Int;
    /**Set the parent for this frame// - Moved in 1.10.**/
    function SetParent(parent : EitherType<Region, String>) : Void;
    /**Set an attachment point of this object// - Updated in 1.10. Since 2.2 cooordinates are now relative to the closest anchor point**/
    function SetPoint(point : Point, ?relative_to : Region, ?relative_point : Point, ?x : Float, ?y : Float) : Void;
    /**Set all anchors to match edges of specified frame// - Moved in 1.10.**/
    function SetAllPoints(region : EitherType<Region, String>) : Void;
    /**Clear all attachment points for this object.**/
    function ClearAllPoints() : Void;

    /**Get the width of this object.**/
    function GetWidth() : Float;
    /**Get the height of this object.**/
    function GetHeight() : Float;
    /**Set the width of the object.**/
    function SetWidth(width : Float) : Void;
    /**Set the height of the object.**/
    function SetHeight(height : Float) : Void;
    /**Sets the width and the height of the object as SetHeight and SetWidth do, but in one function.**/
    function SetSize(width : Float, height : Float) : Void;

    /**Get frame's left, bottom, width, height.**/
    function GetRect() : Rect;
    /**Get the coordinates of the center of this frame// - Moved in 1.10.**/
    function GetCenter() : FloatXY;
    /**Get the x location of the left edge of this frame// - Moved in 1.10.**/
    function GetLeft() : Float;
    /**Get the y location of the top edge of this frame// - Moved in 1.10.**/
    function GetTop() : Float;
    /**Get the x location of the right edge of this frame// - Moved in 1.10.**/
    function GetRight() : Float;
    /**Get the y location of the bottom edge of this frame// - Moved in 1.10.**/
    function GetBottom() : Float;

    /**Set this object to shown (it will appear if its parent is visible).**/
    function Show() : Void;
    /**Set this object to hidden (it and all of its children will disappear).**/
    function Hide() : Void;
    /**Shows or hides the region.**/
    function SetShown(shownFlag : Bool) : Void;
    /**Determine if this object is shown (would be visible if its parent was visible).**/
    function IsShown() : Bool;
    /**Get whether the object is visible on screen (logically (IsShown() and GetParent():IsVisible()))**/
    function IsVisible() : Bool;
    /**Checks whether the mouse is over the frame (or within specified offsets).**/
    function IsMouseOver(?top : Float, ?bottom : Float, ?left : Float, ?right : Float) : Bool;
    /**True if this Region or its Parent is being dragged.// - New in 3.1.0**/
    function IsDragging() : Bool;
    /**Determine if this object can be manipulated in certain ways by tainted code in combat or not**/
    function IsProtected() : Bool;
}

@:multiReturn
extern class Rect {
    var left : Float;
    var bottom : Float;
    var width : Float;
    var height : Float;
}

@:enum
abstract Point(String) {
    var CENTER;
    var TOP;
    var RIGHT; 
    var BOTTOM; 
    var LEFT;
    var TOPLEFT;
    var TOPRIGHT;
    var BOTTOMLEFT;
    var BOTTOMRIGHT;
}

@:multiReturn
extern class PointData {
    var point : Point;
    var relative_to : Region;
    var relative_point : Point;
    var x : Float;
    var y : Float;
}