package wow.widget;

extern class UIObject {
    function GetParent() : UIObject; // - Moved from Region:GetParent(). This is essentially the same as the old version, except that you can no longer assume that your object has a Frame type in its hierarchy somewhere.
    function GetAlpha() : Float; // - Return this object's alpha (opacity) value.
    function GetName() : String; // - Return the name of the object.
    function GetObjectType() : String; // - Get the type of this object.
    function IsForbidden() : Bool; // - Returns whether insecure interaction with a widget is forbidden.
    function IsObjectType(type : String) : Bool; // - Determine if this object is of the specified type, or a subclass of that type.
    function SetAlpha(alpha : Float) : Void;// - Set the object's alpha (opacity) value. 
}