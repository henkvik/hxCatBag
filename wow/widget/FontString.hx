package wow.widget;


import wow.G.FloatXY;
import wow.G.Color;
import wow.widget.FontInstance.FontJustifyH;
import wow.widget.FontInstance.FontJustifyV;
import wow.widget.FontInstance.FontInfo;


extern class FontString extends LayeredRegion {
    function CanNonSpaceWrap() : Bool;      // - Get whether long strings without spaces are wrapped or truncated - New in 1.11.
    function GetStringHeight() : Float;     // - Returns the height in pixels of the current string in the current font (without line wrapping). - New in 2.3.
    function GetStringWidth() : Float;      // - Returns the width in pixels of the current string in the current font (without line wrapping).
    function GetText() : String;            // - Get the displayed text.
    function SetText(text : String) : Void; // - Set the displayed text.
    function SetTextHeight(pixels : Float) : Void;  // - Set the height of the text by scaling graphics (Note: Can distort text). 
    function SetNonSpaceWrap(wrap : Bool) : Void;   // - Set whether long strings without spaces are wrapped or truncated.
    function SetAlphaGradient(start : Float, length : Float) : Void;    // - Create or remove an alpha gradient over the text.
    // function SetFormattedText(format_string : String, [, ...]) : Void;// - Set the formatted display text. - New in 2.3.


    // FONT INSTANCE FUNCTIONS //
    function GetFont() : FontInfo;// - Return the font file, height, and flags.
    function GetFontObject() : Null<FontInstance>;// - Return the 'parent' Font object, or nil if none.
    function GetJustifyH() : Bool;// - Return the horizontal text justification.
    function GetJustifyV() : Bool;// - Return thevertical text justification.
    function GetShadowColor() : Color;// - Returns the color of text shadow (r, g, b, a).
    function GetShadowOffset() : FloatXY;// - Returns the text shadow offset (x, y).
    function GetSpacing() : Float;// - Returns the text spacing.
    function GetTextColor() : Color;// - Returns the default text color.
    function SetFont(file : String, height : Float, ?flags : String) : Void;// - Sets the font to use for text, returns 1 if the path was valid, nil otherwise (no change occurs).
    function SetFontObject(font : FontInstance) : Void;// - Sets the 'parent' Font object from which this object inherits properties.
    function SetJustifyH(justify : FontJustifyH) : Void;// - Sets horizontal text justification ("LEFT","RIGHT", or "CENTER")
    function SetJustifyV(justify : FontJustifyV) : Void;// - Sets vertical text justification ("TOP","BOTTOM", or "MIDDLE")
    function SetShadowColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the text shadow color.
    function SetShadowOffset(x : Float, y : Float) : Void;// - Sets the text shadow offset.
    function SetSpacing(spacing : Float) : Void;// - Sets the spacing between lines of text in the object.
    function SetTextColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the default text color. 
}