package wow.widget;

import wow.G.Color;
import wow.G.FloatXY;

extern class FontInstance extends UIObject {
    function GetFont() : FontInfo;// - Return the font file, height, and flags.
    function GetFontObject() : Null<FontInstance>;// - Return the 'parent' Font object, or nil if none.
    function GetJustifyH() : Bool;// - Return the horizontal text justification.
    function GetJustifyV() : Bool;// - Return thevertical text justification.
    function GetShadowColor() : Color;// - Returns the color of text shadow (r, g, b, a).
    function GetShadowOffset() : FloatXY;// - Returns the text shadow offset (x, y).
    function GetSpacing() : Float;// - Returns the text spacing.
    function GetTextColor() : Color;// - Returns the default text color.
    function SetFont(file : String, height : Float, ?flags : String) : Void;// - Sets the font to use for text, returns 1 if the path was valid, nil otherwise (no change occurs).
    function SetFontObject(font : FontInstance) : Void;// - Sets the 'parent' Font object from which this object inherits properties.
    function SetJustifyH(justify : FontJustifyH) : Void;// - Sets horizontal text justification ("LEFT","RIGHT", or "CENTER")
    function SetJustifyV(justify : FontJustifyV) : Void;// - Sets vertical text justification ("TOP","BOTTOM", or "MIDDLE")
    function SetShadowColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the text shadow color.
    function SetShadowOffset(x : Float, y : Float) : Void;// - Sets the text shadow offset.
    function SetSpacing(spacing : Float) : Void;// - Sets the spacing between lines of text in the object.
    function SetTextColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the default text color. 
}

@:multiReturn
extern class FontInfo {
    var name : String;
    var height : Float;
    var flags : String;
}

@:enum
abstract FontJustifyH(String) {
    var LEFT;
    var RIGHT;
    var CENTER;
}

@:enum
abstract FontJustifyV(String) {
    var TOP;
    var BOTTOM;
    var MIDDLE;
}