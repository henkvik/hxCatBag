package wow.widget;

import haxe.Constraints.Function;
import wow.widget.Texture.TexturePath;
import wow.widget.Texture.MaskTexture;
import wow.widget.Region.Point;
import wow.G.FloatWH;
import wow.G.FloatXY;
import wow.G.Color;
import wow.G.FloatLRTB;
import wow.widget.LayeredRegion.DrawLayer;

@:native("_G")
extern class Frame extends Region {
    /**(["name"[,"layer"[,"inheritsFrom"]]]) - Create and return a new FontString as a child of this Frame - Can instantiate virtuals in 1.11.**/
    function CreateFontString(?name:String, ?layer : DrawLayer, ?templates : String) :FontString;                      
    //**(["name"[,"layer"[,"inheritsFrom"[,subLayer]]]]) - Create and return a new Line as a child of this Frame.**/
    //function CreateLine(?name:String, ?layer : DrawLayer, ?templates : String, ?sub_layer : Int) :Void;                            
    /**(["name"[,"layer"[,"inheritsFrom"[,subLayer]]]]) - Create and return a new MaskTexture as a child of this Frame.**/
    function CreateMaskTexture(?name:String, ?layer : DrawLayer, ?templates : String, ?sub_layer : Int) :MaskTexture;                     
    /**(["name"[,"layer"[,"inheritsFrom"[,subLayer]]]]) - Create and return a new Texture as a child of this Frame.**/
    function CreateTexture(?name:String, ?layer : DrawLayer, ?templates : String, ?sub_layer : Int) :Texture;
    
    /**("layer") - Disable rendering of "regions" (fontstrings, textures) in the specified draw layer.**/
    function DisableDrawLayer(layer : DrawLayer) :Void;                      
    /**("layer") - Enable rendering of "regions" (fontstrings, textures) in the specified draw layer.**/
    function EnableDrawLayer(layer : DrawLayer) :Void;                       
    /**(enableFlag) - Set whether this frame will get keyboard input.**/
    function EnableKeyboard(enable :Bool) :Void;                        
    /**(enableFlag) - Set whether this frame will get mouse input.**/
    function EnableMouse(enable :Bool) :Void;                           
    /**(enableFlag) - Set whether this frame will get mouse wheel notifications.**/
    function EnableMouseWheel(enable :Bool) :Void;                      
    /**(prefix, name, suffix) - Returns the first existing attribute of (prefix..name..suffix), ("*"..name..suffix), (prefix..name.."*"), ("*"..name.."*"), (name).**/
    function GetAttribute(prefix :String, name :String, suffix :String) :Any;                          
    /**() - Creates and returns a backdrop table suitable for use in SetBackdrop - New in 1.11.**/
    function GetBackdrop() :Backdrop;                           
    /**() - Gets the frame's backdrop border color (r, g, b, a)- New in 1.11.**/
    function GetBackdropBorderColor() :Color;                
    /**() - Gets the frame's backdrop color (r, g, b, a)- New in 1.11.**/
    function GetBackdropColor() :Color;                      
    /**() - Gets the modifiers to the frame's rectangle used for clamping the frame to screen.**/
    function GetClampRectInsets() :FloatLRTB;                    
    /**() - New in 3.0.8**/
    function GetDepth() :Float;                              
    /**() - Returns the effective alpha of a frame. - Since 2.1.**/
    function GetEffectiveAlpha() :Float;                     
    /**() - New in 3.0.8**/
    function GetEffectiveDepth() :Float;                     
    /**() - Get the scale factor of this object relative to the root window.**/
    function GetEffectiveScale() :Float;                     
    /**() - Get the level of this frame.**/
    function GetFrameLevel() :Int;                         
    /**() - Get the strata of this frame.**/
    function GetFrameStrata() :FrameStrata;                        
    /**() - Get the type of this frame.**/
    function GetFrameType() :FrameType;                          
    /**() - Gets the frame's hit rectangle inset distances (l, r, t, b) - new in 1.11.**/
    function GetHitRectInsets() :FloatLRTB;                      
    /**() - Get the ID of this frame.**/
    function GetID() :Int;                                 
    /**() - Gets the frame's maximum allowed resize bounds (w, h) - new in 1.11.**/
    function GetMaxResize() :FloatWH;                          
    /**() - Gets the frame's minimum allowed resize bounds (w, h) - new in 1.11.**/
    function GetMinResize() :FloatWH;                          
    /**() - Get the number of "children" (frames and things derived from frames) this frame has.**/
    function GetNumChildren() :Int;                        
    /**() - Return the number of "regions" (fontstrings, textures) belonging to this frame.**/
    function GetNumRegions() :Int;                         
    /**() - Returns if keyboard inputs are being propagated.**/
    function GetPropagateKeyboardInput() :Bool;             
    /**() - Get the scale factor of this object relative to its parent.**/
    function GetScale() :Float;                              
    /**("handler") - Get the function for one of this frame's handlers.**/
    function GetScript(handler :String) :Function;                             
    /**("handler") - Return true if the frame can be given a handler of the specified type (NOT whether it actually HAS one, use GetScript for that) - Since 1.8.**/
    function HasScript(handler :String) :Bool;                             
    /**("handler", function) - Hook a secure frame script.**/
    function HookScript(handler :String, callback :Any) :Void;                            
    /**(ignoreFlag) - New in 3.0.8**/
    function IgnoreDepth(ignore :Bool) :Void;                           
    /**() - Gets whether the frame is prohibited from being dragged off screen.**/
    function IsClampedToScreen() :Bool;                     
    /**("event") - Returns true if the given event is registered to the frame.**/
    function IsEventRegistered(event :String) :Bool;                     
    /**("type") - Determine if this frame is of the specified type, or a subclass of that type.**/
    function IsFrameType(type :FrameType) :Bool;                           
    /**() - New in 3.0.8**/
    function IsIgnoringDepth() :Bool;                       
    /**() - Get whether this frame will get keyboard input. - New in 1.11.**/
    function IsKeyboardEnabled() :Bool;                     
    /**() - Get whether this frame will get mouse input. - New in 1.11.**/
    function IsMouseEnabled() :Bool;                        
    /**() - Get whether this frame will get mouse wheel notifications. New in 1.11.**/
    function IsMouseWheelEnabled() :Bool;                   
    /**() - Determine if the frame can be moved.**/
    function IsMovable() :Bool;                             
    /**() - Determine if the frame can be resized.**/
    function IsResizable() :Bool;                           
    /**() - Get whether the frame is set as toplevel - New in 1.10.2.**/
    function IsToplevel() :Bool;                            
    /**() - Determine if this frame has been relocated by the user.**/
    function IsUserPlaced() :Bool;                          
    /**() - Lower this frame behind other frames.**/
    function Lower() :Void;                                 
    /**() - Raise this frame above other frames.**/
    function Raise() :Void;                                 
    /**() - Register this frame to receive all events (For debugging purposes only!)**/
    function RegisterAllEvents() :Void;                     
    /**("event") - Indicate that this frame should be notified when event occurs.**/
    function RegisterEvent(event :String) :Void;                         
    /**("event", "unit1"[, "unit2"]) - Indicate that this frame should be notified when event occur for specified units only. (New in 5.0.4)**/
    function RegisterUnitEvent(event :String, unit : haxe.extern.Rest<String>) :Void;                     
    /**("buttonType"[,"buttonType"...]) - Inidicate that this frame should be notified of drag events for the specified buttons.**/
    function RegisterForDrag(button_type :haxe.extern.Rest<ButtonType>) :Void;                       
    /**("name", value) - Sets an attribute on the frame.**/
    function SetAttribute(name :String, value: Any) :Void;                          
    /**([backdropTable]) - Set the backdrop of the frame according to the specification provided.**/
    function SetBackdrop(?backdrop :Backdrop) :Void;                           
    /**(r, g, b[, a]) - Set the frame's backdrop's border's color.**/
    function SetBackdropBorderColor(r :Float, g :Float, b :Float, ?a:Float) :Void;                
    /**(r, g, b[, a]) - Set the frame's backdrop color.**/
    function SetBackdropColor(r :Float, g :Float, b :Float, ?a:Float) :Void;                      
    /**(clamped) - Set whether the frame is prohibited from being dragged off screen.**/
    function SetClampedToScreen(clamp :Bool) :Void;                    
    /**(left, right, top, bottom) - Modify the frame's rectangle used to prevent dragging offscreen.**/
    function SetClampRectInsets(left :Float, right :Float, top :Float, bottom :Float) :Void;                    
    /**(clipped) - Set the frame clipping its children.**/
    function SetClipsChildren(clip :Bool) :Void;                      
    /**(depth) - New in 3.0.8**/
    function SetDepth(depth :Float) :Void;                              
    /**(level) - Set the level of this frame (determines which of overlapping frames shows on top).**/
    function SetFrameLevel(level :Int) :Void;                         
    /**("strata") - Set the strata of this frame.**/
    function SetFrameStrata(strata :FrameStrata) :Void;                        
    /**(left, right, top, bottom) - Set the inset distances for the frame's hit rectangle.**/
    function SetHitRectInsets(left :Float, right :Float, top :Float, bottom :Float) :Void;                      
    /**(id) - Set the ID of this frame.**/
    function SetID(id :Int) :Void;                                 
    /**(maxWidth, maxHeight) - Set the maximum dimensions this frame can be resized to.**/
    function SetMaxResize(max_width :Float, max_height :Float) :Void;                          
    /**(minWidth, minHeight) - Set the minimum dimensions this frame can be resized to.**/
    function SetMinResize(min_width :Float, min_height :Float) :Void;                          
    /**(isMovable) - Set whether the frame can be moved.**/
    function SetMovable(movable :Bool) :Void;                            
    /**(propagate) - Sets whether to propagate keyboard input to other frames. Note: Only OnKeyDown will fire and only for non-modifier keys**/
    function SetPropagateKeyboardInput(propagate :Bool) :Void;             
    /**(isResizable) - Set whether the frame can be resized.**/
    function SetResizable(resizable :Bool) :Void;                          
    /**(scale) - Set the scale factor of this frame relative to its parent.**/
    function SetScale(scale :Float) :Void;                              
    /**("handler", function) - Set the function to use for a handler on this frame.**/
    function SetScript(handler :String, callback : Null<Function>) :Void;                             
    /**(isTopLevel) - Set whether the frame should raise itself when clicked - New in 1.10.2.**/
    function SetToplevel(top :Bool) :Void;                           
    /**(isUserPlaced) - Set whether the frame has been relocated by the user (and will thus be saved in the layout cache).**/
    function SetUserPlaced(user_placed :Bool) :Void;                         
    /**() - Start moving this frame.**/
    function StartMoving() :Void;                           
    /**("point") - Start sizing this frame using the specified anchor point.**/
    function StartSizing(point :Point) :Void;                           
    /**() - Stop moving and/or sizing this frame.**/
    function StopMovingOrSizing() :Void;                    
    /**() - Indicate that this frame should no longer be notified when any events occur.**/
    function UnregisterAllEvents() :Void;                   
    /**("event") - Indicate that this frame should no longer be notified when event occurs. **/
    function UnregisterEvent(event :String) :Void;                       
    
    //**() - Return the "regions" (fontstrings, textures) of the frame (multiple return values) belonging to this frame.**
    function GetRegions() :Dynamic;
    /**
    //**() - Create a title region for the frame if it does not have one. - New in 1.11**
    // function CreateTitleRegion() :Void;                     
                         
    //**() - Get the list of "children" (frames and things derived from frames) of this frame.**
    function GetChildren() :;                           
    //**() - Return the frame's title region - New in 1.11.**
    function GetTitleRegion() :;                        
    **/
}


class FrameEx {
    public static function RegisterEvents(frame :Frame, events :Array<String>) {
        for (event in events) {
            frame.RegisterEvent(event);
        }
    }
}

@:enum
abstract FrameType(String) {
    var FRAME;
    var BUTTON;
    var COOLDOWN;
    var COLORSELECT;
    var EDITBOX;
    var GAMETOOLTIP;
    var MESSAGEFRAME;
    var MINIMAP;
    var MODEL;
    var SCROLLFRAME;
    var SCROLLINGMESSAGEFRAME;
    var SIMPLEHTML;
    var SLIDER;
    var STATUSBAR;
}

@:enum
abstract FrameStrata(String) {
    /** Reserved for WorldFrame, cannot be assigned **/ 
    var WORLD;
    var BACKGROUND;
    var LOW;
    var MEDIUM;
    var HIGH;
    var DIALOG;
    var FULLSCREEN;
    var FULLSCREEN_DIALOG;
    var TOOLTIP;
}

@:enum
abstract ButtonType(String) {
    var LeftButton;
    var RightButton;
    var MiddleButton;
    var Button4;
    var Button5;
    var Any;
}



class Backdrop {
    var bgFile :String;
    var edgeFile :String;
    var tile :Bool;
    var tileSize :Float;
    var edgeSize :Float;
    var insets :FloatLRTB;

    public function new(?bgFile :String, ?edgeFile :String, ?edgeSize :Float, ?insets :FloatLRTB) {
        this.bgFile = bgFile;
        this.edgeFile = edgeFile;
        this.edgeSize = edgeSize;
        this.insets = insets;
    }
}