package wow.widget;

extern class LayeredRegion extends Region {
    function GetDrawLayer() : DrawLayer; //- Returns the draw layer for the Region.
    function SetDrawLayer(layer : DrawLayer, ?sublevel : Int) : Void; //- Sets the draw layer for the Region
    function SetVertexColor(r : Float, g : Float, b : Float, ?a : Float) : Void; 
}

@:enum
abstract DrawLayer(String) {
    var BACKGROUND;
    var BORDER;
    var ARTWORK;
    var OVERLAY;
    var HIGHLIGHT;
}