package wow.widget;

import wow.G.FloatLRTB;
import wow.G.Color;
import wow.G.FloatXY;
import wow.widget.FontInstance.FontInfo;
import wow.widget.FontInstance.FontJustifyV;
import wow.widget.FontInstance.FontJustifyH;

extern class EditBox extends Frame {
    
    function AddHistoryLine(text : String) : Void;          //("text") - Add text to the edit history.
    function ClearFocus() : Void;                           //()
    function GetAltArrowKeyMode() : Bool;                   //() - Return whether only alt+arrow keys work for navigating the edit box, not arrow keys alone.
    function GetBlinkSpeed() : Float;                       //() - Gets the blink speed of the EditBox in seconds - New in 1.11.
    function GetCursorPosition() : Int;                     //() - Gets the position of the cursor inside the EditBox - New in 2.3.
    function GetHistoryLines() : Int;                       //() - Get the number of history lines for this edit box
    function GetHyperlinksEnabled() : Bool;                 //() - Returns whether the EditBox triggers hyperlink script handlers.
    function GetInputLanguage() : String;                   //() - Get the input language (locale based not in-game)
    function GetMaxBytes() :Int;                            //() - Gets the maximum number bytes allowed in the EditBox - New in 1.11.
    function GetMaxLetters() :Int;                          //() - Gets the maximum number of letters allowed in the EditBox - New in 1.11.
    function GetNumLetters() :Int;                          //() - Gets the number of letters in the box.
    function GetNumber() :Float;                            //()
    function GetText() :String;                             //() - Get the current text contained in the edit box.
    function GetTextInsets() :FloatLRTB;                    //() - Gets the text display insets for the EditBox - New in 1.11.
    function HasFocus() :Bool;                              //() - Returns whether the edit box is currently being edited (has edit focus).
    function HighlightText(?start :Int, ?end :Int) :Void;   //([startPos, endPos]) - Set the highlight to all or some of the edit box text.
    function Insert(text :String) :Void;                    //(\"text\") - Insert text into the edit box.
    function IsAutoFocus() :Bool;                           //() - Determine if the EditBox has autofocus enabled - New in 1.11.
    function IsMultiLine() :Bool;                           //() - Determine if the EditBox accepts multiple lines - New in 1.11.
    function IsNumeric() :Bool;                             //() - Determine if the EditBox only accepts numeric input - New in 1.11.
    function IsPassword() :Bool;                            //() - Determine if the EditBox performs password masking - New in 1.11.
    function SetAltArrowKeyMode(enable :Bool) :Void;        //(enable) - Make only alt+arrow keys work for navigating the edit box, not arrow keys alone.
    function SetAutoFocus(enable :Bool) :Void;              //(state) - Set whether or not the editbox will attempt to get input focus when it gets shown (default: yes) - New in 1.11.
    function SetBlinkSpeed(seconds :Float) :Void;           //
    function SetCursorPosition(position :Int) :Void;        //(position) - Set the position of the cursor within the EditBox - New in 2.3.
    function SetFocus() :Void;                              //() - Move input focus (the cursor) to this editbox
    function SetHistoryLines(lines :Int) :Void;             //() - Set the number of history lines to remember.
    function SetHyperlinksEnabled(enable :Bool) :Void;      //(enableFlag) - Set whether the EditBox triggers hyperlink script handlers.
    function SetMaxBytes(max :Int) :Void;                   //(maxBytes) - Set the maximum byte size for entered text.
    function SetMaxLetters(max :Int) :Void;                 //(maxLetters) - Set the maximum number of letters for entered text.
    function SetMultiLine(enable :Bool) :Void;              //(state) - Set the EditBox's multi-line state - New in 1.11.
    function SetNumber(numer :Float) :Void;                 //(number)
    function SetNumeric(enable :Bool) :Void;                //(state) - Set if the EditBox only accepts numeric input - New in 1.11.
    function SetPassword(enable :Bool) :Void;               //(state) - Set the EditBox's password masking state - New in 1.11.
    function SetText(text :String) :Void;                   //("text") - Set the text contained in the edit box.
    function ToggleInputLanguage(?any:Any) :Any;            //() 
    function SetTextInsets(left :Float, right :Float, top :Float, bottom :Float) :Void; //(l, r, t, b)





    // FONTINSTANCE FUNCTIONS //
    function GetFont() : FontInfo;// - Return the font file, height, and flags.
    function GetFontObject() : Null<FontInstance>;// - Return the 'parent' Font object, or nil if none.
    function GetJustifyH() : Bool;// - Return the horizontal text justification.
    function GetJustifyV() : Bool;// - Return thevertical text justification.
    function GetShadowColor() : Color;// - Returns the color of text shadow (r, g, b, a).
    function GetShadowOffset() : FloatXY;// - Returns the text shadow offset (x, y).
    function GetSpacing() : Float;// - Returns the text spacing.
    function GetTextColor() : Color;// - Returns the default text color.
    function SetFont(file : String, height : Float, ?flags : String) : Void;// - Sets the font to use for text, returns 1 if the path was valid, nil otherwise (no change occurs).
    function SetFontObject(font : FontInstance) : Void;// - Sets the 'parent' Font object from which this object inherits properties.
    function SetJustifyH(justify : FontJustifyH) : Void;// - Sets horizontal text justification ("LEFT","RIGHT", or "CENTER")
    function SetJustifyV(justify : FontJustifyV) : Void;// - Sets vertical text justification ("TOP","BOTTOM", or "MIDDLE")
    function SetShadowColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the text shadow color.
    function SetShadowOffset(x : Float, y : Float) : Void;// - Sets the text shadow offset.
    function SetSpacing(spacing : Float) : Void;// - Sets the spacing between lines of text in the object.
    function SetTextColor(r : Float, g : Float, b : Float, ?a : Float) : Void;// - Sets the default text color. 
}