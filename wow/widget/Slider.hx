package wow.widget;

import wow.widget.Texture.TexturePath;
import wow.G.Flag;
extern class Slider extends Frame {
    /**() - Disables the slider**/
    public function Disable():Void;
    /**() - Enables the slider**/
    public function Enable():Void;
    /**() - Get the current bounds of the slider.**/
    public function GetMinMaxValues():SliderMinMax;
    /**() - Returns whether the slider constrains values to value steps when dragged.**/
    public function GetObeyStepOnDrag():Flag;
    /**() - Returns "HORIZONTAL" or "VERTICAL".**/
    public function GetOrientation():SliderOrientation;
    /**() - Get the texture for this slider's thumb - New in 1.11.**/
    public function GetThumbTexture():Texture;
    /**() - Get the current value of the slider.**/
    public function GetValue():Float;
    /**() - Get the current step size of the slider.**/
    public function GetValueStep():Float;
    /**() - Returns the number of steps the slider's value changes by when the slider is clicked.**/
    public function GetStepsPerPage():Float;
    /**() - Returns enabled status of the slider.**/
    public function IsEnabled():Bool;
    /**(min, max) - Set the bounds of the slider.**/
    public function SetMinMaxValues(min :Float, max :Float):Void;
    /**(obeyStep) - Sets whether the slider constrains values to to value steps when dragged.**/
    public function SetObeyStepOnDrag(obey:Bool):Void;
    /**("orientation") - "HORIZONTAL" or "VERTICAL".**/
    public function SetOrientation(orientation :SliderOrientation):Void;
    /**(texture or "texturePath")**/
    public function SetThumbTexture(texture:TexturePath):Void;
    /**(value) - Set the value of the slider. Also causes the thumb to show on the first call.**/
    public function SetValue(value:Float):Void;
    /**(value) - Set the step size of the slider.**/
    public function SetValueStep(step:Float):Void;
    /**(steps) - Controls slider behavior when the user clicks within the slider's tracking area. **/
    public function SetStepsPerPage(steps:Float):Void;
}

@:enum
abstract SilderScript(String) to String {
    /** (self, key, value) */
    var ON_ATTRIBUTE_CHANGED = "OnAttributeChanged";
    /** (self,text) */
    var ON_CHAR = "OnChar";
    /** (self,button) */
    var ON_DRAG_START = "OnDragStart";
    /** (self) */
    var ON_DRAG_STOP = "OnDragStop";
    /** (self, motion) */
    var ON_ENTER = "OnEnter";
    /** (self,event,...) */
    var ON_EVENT = "OnEvent";
    /** (self) */
    var ON_HIDE = "OnHide";
    /** (self,key) */
    var ON_KEY_DOWN = "OnKeyDown";
    /** (self,key) */
    var ON_KEY_UP = "OnKeyUp";
    /** (self, motion) */
    var ON_LEAVE = "OnLeave";
    /** (self) */
    var ON_LOAD = "OnLoad";
    /** (self,button) */
    var ON_MOUSE_DOWN = "OnMouseDown";
    /** (self,button) */
    var ON_MOUSE_UP = "OnMouseUp";
    /** (self,delta) */
    var ON_MOUSE_WHEEL = "OnMouseWheel";
    /** (self) */
    var ON_RECEIVE_DRAG = "OnReceiveDrag";
    /** (self) */
    var ON_SHOW = "OnShow";
    /** (self,w,h) */
    var ON_SIZE_CHANGED = "OnSizeChanged";
    /** (self,elapsed) */
    var ON_UPDATE = "OnUpdate";
    /** (self,value)  */
    var ON_VALUE_CHANGED = "OnValueChanged";
}

@:enum
abstract SliderOrientation(String) {
    var HORIZONTAL;
    var VERTICAL;
}

@:multiRetuern
extern class SliderMinMax {
    var min : Float;
    var max : Float;
}