package wow.widget;

extern class Font extends FontInstance {
    function CopyFontObject(font : Font) : Void; // - Set this Font's attributes to be a copy of the otherFont font object's.
}