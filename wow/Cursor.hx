package wow;

import wow.widget.Texture.TexturePath;
import haxe.extern.EitherType;
import wow.G.FloatXY;
@:native("_G")
extern class Cursor {

    static function AutoEquipCursorItem() :Void;            //() - Causes the equipment on the cursor to be equipped.
    static function ClearCursor() :Void;                    //() - Clears whatever item the cursor is dragging from the cursor. - Added in 1.12
    static function CursorCanGoInSlot(inventory_id : Int) :Void;              //(invSlot) - Return true if the item currently held by the cursor can go into the given inventory (equipment) slot.
    static function CursorHasItem() :Bool;                  //() - Returns true if the cursor currently holds an item.
    static function CursorHasMoney() :Bool;                 //() - Returns true if the cursor currently holds money.
    static function CursorHasSpell() :Bool;                 //() - Returns true if the cursor currently holds a spell.
    static function DeleteCursorItem() :Void;               //() - Destroys the item on the cursor.
    static function DropCursorMoney() :Void;                //() - Drops the amount of money held by the cursor.
    static function DropItemOnUnit(unit : String) :Void;                 //("unit") - Drops an item from the cursor onto a unit.
    static function EquipCursorItem(inventory_id : Int) :Void;                //(invSlot) - Equips the currently picked up item to a specific inventory slot.
    // static function GetCursorInfo() : Void;                  //() - Returns information about what the cursor is holding.
    static function GetCursorMoney() :Int;                 // - Returns the amount of money held by the cursor.
    static function GetCursorPosition() :FloatXY;              //() - Returns the cursor's position on the screen.
    static function HideRepairCursor() :Void;               //() - Hides the repair cursor.
    static function InRepairMode() :Bool;                   //() - Returns true if your cursor is in repair mode.
    static function PickupAction(slot :Int) :Void;                       //NOCOMBAT (slot) - Drags an action out of the specified quickbar slot and holds it on the cursor.
    static function PickupBagFromSlot(slot:Int) :Void;              //(slot) - Picks up the bag from the specified slot, placing it in the cursor. If an item is already picked up, this places the item into the specified slot, swapping the items if needed.
    static function PickupContainerItem(bag:Int, slot:Int) :Void;            //(bagID, slot) -
    static function PickupInventoryItem(inventory_id : Int) :Void;            //(invSlot) - "Picks up" an item from the player's worn inventory.
    static function PickupItem(item : EitherType<Int, String>) :Void;                     //(itemId or "itemString" or "itemName" or "itemLink")
    static function PickupMacro(macro_id : EitherType<Int, String>) :Void;                       // NOCOMBAT("macroName" or index) - Places the specified macro onto the cursor.
    static function PickupMerchantItem(index :Int) :Void;             //(index) - Places the item onto the cursor. If the cursor already has an item, the item in the cursor will be sold.
    static function PickupPetAction(slot:Int) :Void;                       // NOCOMBAT(slot) - Drags an action from the specified pet action bar slot into the cursor.
    static function PickupPlayerMoney(amount :Int) :Void;              // - Picks up an amount of money from the player.
    static function PickupSpell(spell_id :Int) :Void;                       // NOCOMBAT(spellID) - Places the specified spell onto the cursor.
    static function PickupStablePet(index :Int) :Void;                //(index) - ?.
    static function PickupTradeMoney(amount:Int) :Void;               //(amount)
    static function PlaceAction(slot:Int) :Void;                    //(slot) - Drops an action from the cursor into the specified quickbar slot.
    static function PutItemInBackpack() :Void;              //() - attempts to place item in backpack (bag slot 0).
    static function PutItemInBag(inventory_id : Int) :Void;                   //(inventoryId) - attempts to place item in a specific bag.
    static function ResetCursor() :Void;                    //() -
    static function SetCursor(cursor :TexturePath) :Void;                      //("cursor") - Path to a texture to use as the cursor image (must be 32x32 pixels) or one of the built-in cursor tokens or nil.
    static function ShowContainerSellCursor(index:Int, slot:Int) :Void;        //(index, slot) -
    static function ShowInspectCursor() :Void;                             // UI () - Change the cursor to the magnifying glass inventory inspection cursor.
    static function ShowInventorySellCursor() :Void;        //() - ?.
    // static function REMOVED() :Void;                        // ShowMerchantSellCursor() - Changes the cursor to the merchant sell cursor.
    static function ShowRepairCursor() :Void;               //() -
    static function SplitContainerItem(bag:Int, slot:Int, amount:Int) :Void;             //(bagID, slot, amount) - Picks up part of a stack. 
}