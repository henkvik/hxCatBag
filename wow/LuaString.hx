package wow;

@:native("string")
extern class LuaString {
    static function format(format : String, args:haxe.extern.Rest<Any>) : String;

}