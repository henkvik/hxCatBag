package wow;

import wow.widget.Texture.TexturePath;
@:native("_G")
extern class Bag {
    
    static function PutItemInBackpack() : Void;     // - attempts to place item in backpack (bag slot 0).
    static function ToggleBackpack() : Void;     //UI  - Toggles your backpack open/closed.
    static function OpenAllBags() : Void;        //UI  - Open all bags
    static function CloseAllBags() : Void;       //UI  - Close all bags
    static function ToggleBag(bagID : Int) : Void;     //UI  - Opens or closes the specified bag.
    static function GetBagName(bagID : Int) : String;       // - Get the name of one of the player's bags.
    static function PickupBagFromSlot(inventoryId : Int) : Void;     // - Picks up the bag from the specified slot, placing it in the cursor.
    static function PutItemInBag(inventoryId : Int) : Void;       // - attempts to place item in a specific bag.

    static function ContainerIDToInventoryID(bagID : Int) : Int;
    static function GetContainerItemCooldown(bagID : Int, slot : Int) : BagItemCooldown; 
    static function GetContainerItemDurability(bagID : Int, slot : Int) : BagItemDurability;       // - Get current and maximum durability of an item in the character's bags.
    static function GetContainerItemID(bagID : Int, slot : Int) : Int;       // - Returns the item ID of the item in a particular container slot.
    static function GetContainerItemInfo(bagID : Int, slot : Int) : BagItemInfo;       // - Get the info for an item in one of the player's bags.
    static function GetContainerItemLink(bagID : Int, slot : Int) : String;       // - Returns the itemLink of the item located in bag#, slot#.
    static function GetContainerNumSlots(bagID : Int) : Int;     // - Returns the total number of slots in the bag specified by the index.
    static function GetContainerItemQuestInfo(bagID : Int, slot : Int) : BagItemQuestInfo;        // - Returns information about quest and quest-starting items in your bags. (added in 3.3.3)
    static function GetContainerNumFreeSlots(bagID : Int) : BagEmptySlots;     // - Returns the number of free slots and type of slots in the bag specified by the index. (added in Patch 2.4)
    static function PickupContainerItem(bagID : Int, slot : Int) : Void;
    static function SplitContainerItem(bagID : Int, slot : Int, amount : Int) : Void;
    static function UseContainerItem(bagID : Int, slot : Int, ?target : String) : Void;     //PROTECTED (situational)  -   Performs a "right click" action on, or targeted use of, on an item in bags. -- 3rd argument added in 1.12 

    static function HandleModifiedItemClick(link:String) : Bool;
}

@:multiReturn
extern class BagItemCooldown {
    var start    : Float;
    var duration : Float;
    var enabled  : Int;
}
@:multiReturn
extern class BagItemDurability {
    var current : Int;
    var maximum : Int;
}

@:multiReturn
extern class BagItemInfo {
    var texture    : TexturePath;  // - the texture for the item in the specified bag slot
    var count      : Int;  // - the number of items in the specified bag slot
    var locked     : Bool;  // - 1 if the item is locked by the server, nil otherwise.
    var quality_id : Int;  // - the numeric quality of the item
    var readable   : Bool;  // - 1 if the item can be "read" (as in a book), nil otherwise
    var lootable   : Bool;  // - true if the item is a temporary container containing items that can be looted
    var link       : String;  // - the itemLink of the item in the specified bag slot
    var filtered   : Bool;  // - true if the item is greyed-out during the current inventory search 
}

@:multiReturn
extern class BagItemQuestInfo {
    var quest_item   : Bool;// - true if the item is a quest item, nil otherwise.
    var quest_id     : Int;// - Quest ID of the quest this item starts, no value if it does not start a quest.
    var quest_active : Bool;// - 1 if the quest this item starts has been accepted by the player, nil otherwise. 
}

@:multiReturn
extern class BagEmptySlots {
    var num  : Int;
    var type : BagType;
}


@:enum 
abstract BagType(Int) {
    /**(for bags it means any item, for items it means no special bag type)**/
    var NORMAL         = 0;
    var QUIVER         = 1;
    var AMMOPOUCH      = 2;
    var SOUL           = 4;
    var LEATHERWORKING = 8;
    var INSCRIPTION    = 16;
    var HERB           = 32;
    var ENCHANTING     = 64;
    var ENGINEERING    = 128;
    var KEYRING        = 256;
    var GEM            = 512;
    var MINING         = 1024;
    var UNKNOWN        = 2048;
    var VANITYPETS     = 4096;
}