package wow;

@:native("string")
extern class StringApi {
    static function gsub(string :String, pattern:String, replace:String, ?offset:Int):String;
}