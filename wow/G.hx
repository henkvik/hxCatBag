package wow;

import lua.Table.AnyTable;
import haxe.Constraints.Function;
import haxe.extern.EitherType;
import wow.widget.Frame;
import wow.widget.Font;
import wow.widget.GameTooltip;
import wow.widget.Frame.FrameType;
import haxe.macro.Compiler;


@:native("_G")
extern class G {
    static function __init__() : Void {
        Compiler.includeFile("wow/preamble.lua");
    }

    static var UIParent(default, null) : Frame;
    static var GameTooltip : GameTooltip;

    static function CreateFrame(type : FrameType, ?name : String, ?parent : Frame, ?templates : String, ?id : Int) : Any;
    static function CreateFont(name :String) : Font;
    static function print(msg : haxe.extern.Rest<String>) :Void;

    static function GetCursorPosition() : FloatXY;
    static function GetMoney() : Int;


    static function loadstring(string : String, ?name :String) : LoadStringVal;   
    static function pcall(func :Any, params :haxe.extern.Rest<Any>) : PCallVal;

    static function IsModifiedClick() : Bool;
    static function InCombatLockdown() : Bool;
    static function GetTime() : Float;

    static function tonumber(string:String, ?base:Int):Null<Float>;

}

abstract Flag(Int) {
    @:to
    public function to_bool() return this == 1;

}


@:multiReturn
extern class Err {
    var file : String;
    var line : Int;
    var error : String;
}

@:multiReturn 
extern class Color {
    var r : Float;
    var g : Float;
    var b : Float;
    var a : Float;
}

@:multiReturn
extern class FloatXY {
    var x : Float;
    var y : Float;
}

@:multiReturn
extern class FloatWH {
    var width : Float;
    var height : Float;
}

@:multiReturn
extern class FloatLRTB {
    var left : Float;
    var right : Float;
    var top : Float;
    var bottom : Float;
}

@:multiReturn
extern class LoadStringVal {
    var func : Null<Function>;
    var err  : String;
}

@:multiReturn 
extern class PCallVal {
    var success : Bool;
    var value : EitherType<String, Any>;
}