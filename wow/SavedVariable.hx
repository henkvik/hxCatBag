package wow;


import lua.Table.AnyTable;

@:forward
abstract SavedVariable(AnyTable)  {
    public static function Load(name : String) : SavedVariable {
        untyped __lua__("_G[{0}] = _G[{0}] or {}", name);
        var saved = untyped __lua__("_G[{0}]", name);
        return saved;
    }
}