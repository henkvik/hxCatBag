package wow;

import lua.Table.AnyTable;

@:native("ADDON")
extern class Addon {
    public static var name(default, never) : String;
    public static var table(default, never) : AnyTable;

}