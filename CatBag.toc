## Interface: 80000

## Title: CatBag
## Notes: Bag replacment based entierly around the concept of user defined categories
## Author: Henrik
## Version: 1.1.0

# SavedVariables: CatBagSavedVariables
## SavedVariablesPerCharacter: CatBagCharacterSavedVariables

# Dependencies
IconTable.lua

# main
bin/main.lua